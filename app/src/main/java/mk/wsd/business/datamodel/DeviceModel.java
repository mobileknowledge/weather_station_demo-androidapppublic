package mk.wsd.business.datamodel;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.Parcelable;
import java.util.List;
import no.nordicsemi.android.support.v18.scanner.ScanResult;

/**
 * Class used to represent a BLE device for the application. It defines additional parameters for a
 * BluetoothDevice from the android library.
 */
public class DeviceModel implements Parcelable {
    private final BluetoothDevice device;
    private String name;
    private int rssi;
    private String deviceType;
    private List<ParcelUuid> serviceUuidList;

    public static final Creator<DeviceModel> CREATOR = new Creator<DeviceModel>() {
        public DeviceModel createFromParcel(Parcel source) {
            return new DeviceModel(source);
        }

        public DeviceModel[] newArray(int size) {
            return new DeviceModel[size];
        }
    };

    /**
     * Constructor with a ScanResult
     * @param scanResult Input parameter from the discovered device in Scan
     */
    public DeviceModel(ScanResult scanResult) {
        this.device = scanResult.getDevice();
        this.name = scanResult.getScanRecord().getDeviceName();
        this.rssi = scanResult.getRssi();
        this.serviceUuidList = scanResult.getScanRecord().getServiceUuids();
    }

    /**
     * Constructor using the Parcel
     */
    private DeviceModel(Parcel parcelIn) {
        this.device = parcelIn.readParcelable(BluetoothDevice.class.getClassLoader());
        this.name = parcelIn.readString();
        this.rssi = parcelIn.readInt();
        this.deviceType = parcelIn.readString();
        this.serviceUuidList = parcelIn.createTypedArrayList(ParcelUuid.CREATOR);
    }

    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeParcelable(this.device, flags);
        parcel.writeString(this.name);
        parcel.writeInt(this.rssi);
        parcel.writeString(this.deviceType);
        parcel.writeTypedList(this.serviceUuidList);
    }

    public int describeContents() {
        return 0;
    }

    public BluetoothDevice getDevice() {
        return this.device;
    }

    public String getAddress() {
        return this.device.getAddress();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeviceType() {
        return this.deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public List<ParcelUuid> getServiceUuidList() {
        return this.serviceUuidList;
    }

    public boolean equals(Object o) {
        if (o instanceof DeviceModel) {
            DeviceModel that = (DeviceModel)o;
            return this.device.getAddress().equals(that.device.getAddress());
        } else {
            return super.equals(o);
        }
    }
}
