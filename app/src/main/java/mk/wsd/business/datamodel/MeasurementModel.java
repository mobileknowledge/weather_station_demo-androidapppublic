package mk.wsd.business.datamodel;

import android.text.format.DateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Class used to define a Measurement object that represents one single measurement received from
 * the RapidIoT device. It consists of a float to store the actual value and a long variable with
 * the timestamp when the measurement was taken.
 */
public class MeasurementModel {

    private float measurementValue;
    private long timeStamp;

    public MeasurementModel(float value){
        this.measurementValue = value;
        this.timeStamp = System.currentTimeMillis();
    }

    public float getValue(){
        return measurementValue;
    }

    public String getTimeStamp(){
        return getDate(timeStamp);
    }

    public long getTimeStampLong(){
        return timeStamp;
    }

    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format("HH:mm:ss", cal).toString();
    }
}
