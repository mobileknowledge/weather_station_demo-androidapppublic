package mk.wsd.business.datamodel;

import java.io.Serializable;

import mk.wsd.R;

/**
 * Enum class with all the pages used in the tutorial and the related layouts
 */
public enum TutorialModel implements Serializable {

    FIRST(1, R.layout.tutorial_page1),
    SECOND(2, R.layout.tutorial_page2),
    THIRD(3, R.layout.tutorial_page3),
    FOURTH(4, R.layout.tutorial_page4),
    FIFTH(5, R.layout.tutorial_page5);

    private int titleResId;
    private int layoutResId;

    TutorialModel(int titleId, int layoutId) {
        titleResId = titleId;
        layoutResId = layoutId;
    }

    public int getTitleResId() {
        return titleResId;
    }

    public int getLayoutResId() {
        return layoutResId;
    }
}

