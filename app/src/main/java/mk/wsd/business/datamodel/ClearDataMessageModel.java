package mk.wsd.business.datamodel;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Class used to generate an object with all the parameters needed for a ClearData message to the
 * cloud. It can also generate the JSON string so the ClearDatabase message can be send and
 * interpretate in the backend
 */
public class ClearDataMessageModel {

    private String sessionID;

    public ClearDataMessageModel(String sessionID){
        this.sessionID = sessionID;
    }

    /**
     * Method to generate the JSON string out of the class information
     * @return String with JSON structure
     */
    public String generateJSON (){
        JSONObject jClearCloud = new JSONObject();
        String message = "";

        try {
            String sessionIDTAG = "SessionID";
            jClearCloud.put(sessionIDTAG, sessionID);
            message = jClearCloud.toString();

        }catch(JSONException e){
            e.printStackTrace();
        }
        return message;
    }
}
