package mk.wsd.business.datamodel;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Class used to generate an object with all the parameters needed for a PostData message to the
 * cloud. It can also generate the JSON string so the PostData message can be send and
 * interpretate in the backend
 */
public class PostDataMessageModel {

    private String name, bleMACAddress, sessionID;
    private float temperature, humidity, pressure, light;
    private long timestamp;
    private String nameTAG = "name";
    private String bleMACAddressTAG = "bleMACAddress";
    private String sessionIDTAG = "sessionID";
    private String measurementTAG = "measurement";
    private String temperatureTAG = "temperature";
    private String humidityTAG = "humidity";
    private String pressureTAG = "pressure";
    private String lightTAG = "light";
    private String timestampTAG = "timestamp";


    public PostDataMessageModel(String nameValue, String macValue, String sessionID, MeasurementSetModel measurementSet){
        name = nameValue;
        bleMACAddress = macValue;
        this.sessionID =  sessionID;
        temperature = measurementSet.getTemperatureValue();
        humidity = measurementSet.getHumidityValue();
        pressure = measurementSet.getPressureValue();
        light = measurementSet.getLightValue();
        timestamp = measurementSet.getTimeStamp()/1000;
    }

    public String generateJSON (){
        JSONObject jBrokerMessage = new JSONObject();
        JSONObject jMeasurement = new JSONObject();
        String brokerMessage = "";

        try {
            jMeasurement.put(humidityTAG, humidity);
            jMeasurement.put(temperatureTAG, temperature);
            jMeasurement.put(lightTAG, light);
            jMeasurement.put(timestampTAG, timestamp);
            jMeasurement.put(pressureTAG, pressure);

            jBrokerMessage.put(bleMACAddressTAG, bleMACAddress);
            jBrokerMessage.put(sessionIDTAG, sessionID);
            jBrokerMessage.put(measurementTAG, jMeasurement);
            jBrokerMessage.put(nameTAG, name);

            brokerMessage = jBrokerMessage.toString();

        }catch(JSONException e){
            e.printStackTrace();
        }
        return brokerMessage;
    }
}
