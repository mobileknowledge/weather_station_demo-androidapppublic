package mk.wsd.business.datamodel;

/**
 * Class defining a single set of measurements that can be uploaded to the cloud. It gathers a measurement value
 * from every measurement type available in the demo so it can be stored in the cloud database.
 */
public class MeasurementSetModel {

    private float temperatureValue, humidityValue, pressureValue, lightValue;
    private long timeStamp;

    public MeasurementSetModel(MeasurementModel temperature, MeasurementModel humidity, MeasurementModel pressure, MeasurementModel light){
        temperatureValue = temperature.getValue();
        humidityValue = humidity.getValue();
        pressureValue = pressure.getValue();
        lightValue = light.getValue();
        timeStamp = (temperature.getTimeStampLong() + humidity.getTimeStampLong() + pressure.getTimeStampLong() + light.getTimeStampLong())/4;
    }

    float getTemperatureValue() {
        return temperatureValue;
    }

    float getHumidityValue() {
        return humidityValue;
    }

    float getPressureValue() {
        return pressureValue;
    }

    float getLightValue() {
        return lightValue;
    }

    long getTimeStamp() {
        return timeStamp;
    }

}
