package mk.wsd.business.mqtt;

import android.content.Context;
import android.util.Log;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import java.util.Properties;
import java.util.Random;
import mk.wsd.business.bluetooth.WSDBleManager;
import mk.wsd.business.datamodel.ClearDataMessageModel;
import mk.wsd.business.datamodel.PostDataMessageModel;
import mk.wsd.business.utils.MeasurementManager;

import static mk.wsd.business.activities.BLEActivity.APPTAG;

/**
 * The MQManager is a singleton class designed to control the communication with the MQTT broker
 */
public class MQManager {

    // Weather Station Demo specific credentials and parameters
    private String clientId = "*****";
    private final String serverURI = "ssl://*****:*****" ;
    private final String publishDataTopic = "*****";
    private final String clearDataTopic = "*****";
    private static final String userName = "*****";
    private static final String password = "*****";

    private Context mContext;
    private static MQManager mInstance = null;
    private boolean mqttconnected = false;
    private MqttClient client;


    public MQManager(final Context context) {
        mContext = context;
    }

    public static synchronized MQManager getInstance(final Context context) {
        if (mInstance == null) {
            mInstance = new MQManager(context);
        }
        return mInstance;
    }

    /**
     * Method to connect to the MQTT broker
     */
    public void connect(){
        // We configure here the connection to the MQTT broker
        MqttConnectOptions connectOptions = new MqttConnectOptions();
        connectOptions.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1_1);
        connectOptions.setAutomaticReconnect(false);
        connectOptions.setCleanSession(false);
        connectOptions.setUserName(userName);
        connectOptions.setPassword(password.toCharArray());
        connectOptions.setWill(publishDataTopic,"/will".getBytes(), 0,false);
        java.util.Properties sslClientProperties = new Properties();
        sslClientProperties.setProperty("com.ibm.ssl.protocol", "SSL");
        connectOptions.setSSLProperties(sslClientProperties);

        clientId = clientId + new Random().nextInt(1000);   // Randomize the clientId user for the MQTT connection

        try{
            // We create the MQTT client, set the callback and connect
            if(client==null || !client.isConnected()) {
                client = new MqttClient(serverURI, clientId, new MemoryPersistence());
                client.setCallback(mqttCallback);
                client.connect(connectOptions);
            }
        }catch(MqttException exception){
            Log.d(APPTAG, "MQManager - Exception!");
            exception.printStackTrace();
        }
    }

    /**
     * Method to disconnect from the MQTT broker
     */
    public void disconnect(){
        try {
            if(client!=null && client.isConnected()) {
                client.disconnect();
                client.close();
                mqttconnected = false;
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


    /**
     * Method to send an input string to the data posting queue of the MQTT broker
     * @param message String with the message that will be send
     */
    public void publishMessage(String message) {
        MqttMessage msg = new MqttMessage();
        msg.setPayload(message.getBytes());
        try {
            client.publish(publishDataTopic, msg.getPayload(), 2, false);
        } catch (MqttException e) {
            e.printStackTrace();
            Log.d(APPTAG, "Publish Exception! " + e.getMessage());
        }
    }

    /**
     * Method to publish a clear data message to the specific topic in the broker.
     */
    public void clearDataFromCloud() {
        MqttMessage msg = new MqttMessage();
        WSDBleManager bleManager = WSDBleManager.getInstance(mContext);

        if(!mqttconnected)
            connect();

        ClearDataMessageModel clearDataMessage = new ClearDataMessageModel(bleManager.getSessionID());  // We create the cleardatamodel
        msg.setPayload(clearDataMessage.generateJSON().getBytes());                                     // and generate the JSON message

        try {
            client.publish(clearDataTopic, msg.getPayload(), 2, false);
        } catch (MqttException e) {
            e.printStackTrace();
            Log.d(APPTAG, "Publish Exception! " + e.getMessage());
        }
    }

    /**
     * Method to publish the last measurement received from the RapidIoT device
     * @return true in case we could achieve to send the data to the broker, false otherwise
     */
    public boolean publishLastMeasurement() {
        MqttMessage msg = new MqttMessage();

        // We create an instance of the MeasurementManager and the WSDBLEManager to obtain the required info
        MeasurementManager measurementManager = MeasurementManager.getInstance();
        WSDBleManager bleManager = WSDBleManager.getInstance(mContext);

        // We check that we have a complete set of measurements to post
        if(measurementManager.isMeasurementSetComplete()) {
            // We retrieve the MAC address and format it to fit the structure defined for the database
            String macAddress = bleManager.getDeviceMAC().replace(":", "");
            macAddress = macAddress.substring(0,6) + "0000" + macAddress.substring(6, macAddress.length());

            // Create the PostDataMessage object to generate the JSON string
            PostDataMessageModel myBrokerMessage = new PostDataMessageModel(bleManager.getDeviceName(),
                                                                        macAddress,
                                                                        bleManager.getSessionID(),
                                                                        measurementManager.getLatestSetOfMeasurements());
            msg.setPayload(myBrokerMessage.generateJSON().getBytes());

            try {
                // We publish the message to the android queue from the broker
                client.publish(publishDataTopic, msg.getPayload(), 2, false);
            } catch (MqttException e) {
                e.printStackTrace();
                Log.d(APPTAG, "Publish Exception! " + e.getMessage());
                return false;
            }
        }
        else{
            // If we don't have a complete set we don't publish
            return false;
        }
        return true;
    }

    /**
     * MQTT Callback methods to control the communication with the broker
     */
    MqttCallbackExtended mqttCallback = new MqttCallbackExtended() {
        @Override
        public void connectionLost(Throwable cause) {
            mqttconnected = false;  // Update the boolean to keep track of the connection status
        }

        @Override
        public void connectComplete(boolean reconnect, String serverURI) {
            mqttconnected = true;  // Update the boolean to keep track of the connection status
        }

        @Override
        public void messageArrived(String topic, MqttMessage message) {
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {
        }
    };

    public boolean isConnected (){
        return mqttconnected;
    }

}
