package mk.wsd.business.bluetooth;

import java.util.List;

import mk.wsd.business.datamodel.DeviceModel;

/**
 * Interface class with the methods that should be implemented when scanning for BLE devices
 */
public interface WSDBleScanListener {
    /**
     * When a certain device is discovered while scanning
     * @param device Device that is discovered
     */
    void onDeviceScanned(DeviceModel device);

    /**
     * When the scan is finished, a list of all devices discovered is displayed
     * @param list List of all devices discovered
     */
    void onDeviceScanTimeOut(List<DeviceModel> list);
}