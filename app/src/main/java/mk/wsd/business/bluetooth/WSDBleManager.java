package mk.wsd.business.bluetooth;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.util.Log;
import mk.wsd.R;
import mk.wsd.business.datamodel.DeviceModel;
import mk.wsd.business.datamodel.MeasurementModel;
import mk.wsd.business.utils.MeasurementManager;
import mk.wsd.business.utils.MeasurementUtils;
import no.nordicsemi.android.ble.BleManagerCallbacks;
import no.nordicsemi.android.support.v18.scanner.ScanSettings;
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat;
import no.nordicsemi.android.support.v18.scanner.ScanCallback;
import no.nordicsemi.android.support.v18.scanner.ScanFilter;
import no.nordicsemi.android.support.v18.scanner.ScanResult;
import no.nordicsemi.android.ble.BleManager;
import no.nordicsemi.android.ble.Request;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import static android.content.Context.MODE_PRIVATE;
import static mk.wsd.business.activities.BLEActivity.APPTAG;

/**
 * The WSDBleManager is a singleton class used to control all communication with the RapidIoT device
 * and some additional task/parameters that are related to the connection like the generation of the
 * sessionID.
 */
public class WSDBleManager extends BleManager<BleManagerCallbacks> {

    // UUIDs of the RapidIoT, the service and the characteristics of every measurement type
    private static final UUID WSDemoUUID = UUID.fromString("01ff5550-ba5e-f4ee-5ca1-eb1e5e4b1ce0");
    private static final UUID WSDemoServiceUUID = UUID.fromString("0ab5b670-c2ce-c4ab-e711-6ccbaa65c888");
    private static final UUID PressureUUID = UUID.fromString("00002A6D-0000-1000-8000-00805F9B34FB");
    private static final UUID TemperatureUUID = UUID.fromString("00002A1C-0000-1000-8000-00805F9B34FB");
    private static final UUID HumidityUUID = UUID.fromString("00002A6F-0000-1000-8000-00805F9B34FB");
    private static final UUID LightUUID = UUID.fromString("0ab5b672-c2ce-c4ab-e711-6ccbaa65c888");

    private static final int SessionIDLength = 6;   // Length of the SessionID that will be generated in every connection

    private Context mContext;
    private volatile boolean isDeviceConnected;
    static private WSDBleManager mInstance = null;

    // Global variables to keep track of every Bluetooth characteristic
    private BluetoothGattCharacteristic mPressureCharacteristic, mHumidityCharacteristic, mTemperatureCharacteristic, mLightCharacteristic;


    private DeviceModel mDevice;    // Device that we are connected or want to connect to
    private WSDBleScanListener mBleScanListener;
    private List<DeviceModel> scanListDevices;  // List with all the devices discovered in the scan
    private BluetoothLeScannerCompat bluetoothLeScanner;
    private ScanCallback scanCallback;      // Current callback for the scanning phase
    private boolean mScanning;  // boolean to check if the app is scanning

    private static Handler mHandlerScan = new Handler();
    private Runnable mRunnableScan = this::stopScan;
    private long refreshTime = 1000;    // Default value for the refresh time configured in the settings menu
    private String sessionID = null;    // ID of the session linked to the current connection


    private WSDBleManager(final Context context) {
        super(context);
        mContext = context;
        isDeviceConnected = false;
    }

    /**
     * To get the callbacks methods assigned.
     * @return BleManagerCallbacks
     */
    @NonNull
    @Override
    protected BleManagerGattCallback getGattCallback() {
        return mGattCallback;
    }

    @Override
    protected boolean shouldAutoConnect() {
        // If you want to connect to the device using autoConnect flag = true, return true here.
        return super.shouldAutoConnect();
    }

    /**
     * Reload the refresh rate from the SharedPreferences to update the current variable
     */
    public void updateRefreshRate(){
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(mContext.getResources().getString(R.string.sharedPref_name), MODE_PRIVATE);
        int defRefreshRateValue = mContext.getResources().getInteger(R.integer.defaultRefreshRateInMillis);
        refreshTime = mSharedPreferences.getInt(mContext.getResources().getString(R
                .string.sharedPref_key_refreshrate), defRefreshRateValue);
    }

    public static synchronized WSDBleManager getInstance(final Context context) {
        if (mInstance == null) {
            mInstance = new WSDBleManager(context);
        }
        return mInstance;
    }

    /**
     * Definition of the BleManager callbacks
     */
    private final BleManagerGattCallback mGattCallback = new BleManagerGattCallback() {

        /**
         * This method returns a list of requests needed to initialize the profile. In our case we
         * enqueue the request to enable the notifications for every sensor characteristic
         * @param gatt The gatt device with services discovered
         * @return The queue of requests
         */
        @Override
        protected Deque<Request> initGatt(final BluetoothGatt gatt) {
            final LinkedList<Request> requests = new LinkedList<>();

            // We enqueue the notification request for every sensor characteristic
            requests.push(Request.newEnableNotificationsRequest(mPressureCharacteristic));
            requests.push(Request.newEnableNotificationsRequest(mHumidityCharacteristic));
            requests.push(Request.newEnableNotificationsRequest(mTemperatureCharacteristic));
            requests.push(Request.newEnableNotificationsRequest(mLightCharacteristic));
            return requests;
        }

        /**
         * This method should return true when the gatt device supports the required services.
         * In our case we have to check that all sensor characteristics are available
         *
         * @param gatt the gatt device with services discovered
         * @return true when the device has the required service
         */
        @Override
        public boolean isRequiredServiceSupported(final BluetoothGatt gatt) {

            // We check if the WSD service is present
            BluetoothGattService service = gatt.getService(WSDemoServiceUUID);

            if (service != null) {
                // If the WSD service is found we have to check that all characteristics
                // are also available
                mPressureCharacteristic = service.getCharacteristic(PressureUUID);
                mHumidityCharacteristic = service.getCharacteristic(HumidityUUID);
                mTemperatureCharacteristic = service.getCharacteristic(TemperatureUUID);
                mLightCharacteristic = service.getCharacteristic(LightUUID);
            }


            return (mPressureCharacteristic != null) &&
                    (mHumidityCharacteristic != null) &&
                    (mTemperatureCharacteristic != null) &&
                    (mLightCharacteristic != null);
        }

        /**
         * This method should nullify all services and characteristics of the device.
         */
        @Override
        protected void onDeviceDisconnected() {
            mPressureCharacteristic = null;
            mHumidityCharacteristic = null;
            mTemperatureCharacteristic = null;
            mLightCharacteristic = null;

            sessionID = null;
            disconnectDevice();

            // We stop posting data when the device gets disconnected
            MeasurementManager.getInstance().stopPostDataService(mContext);
        }


        /**
         * Callback indicating a notification has been received. We have to check from which
         * characteristic we are receiving the notification and proceed accordingly
         *
         * @param gatt           GATT client
         * @param characteristic Characteristic from which the notification came.
         */
        @Override
        public void onCharacteristicNotified(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            // We compare the characteritic where the notification is coming from.
            // Then we extract the data and check if we have to process the data because of the
            // refresh rate chosen by the user.
            if(characteristic.getUuid().equals(PressureUUID)){
                byte[] rawpressure =  characteristic.getValue();
                MeasurementModel measurement = new MeasurementModel(MeasurementUtils.getPressureMeasurement(rawpressure));
                if(fitsRefreshRate(MeasurementManager.TYPE_PRESSURE)){
                    getMeasurementManager().addPressure(measurement);
                }
            }
            else if(characteristic.getUuid().equals(HumidityUUID)){
                byte[] rawhumidity =  characteristic.getValue();
                MeasurementModel measurement = new MeasurementModel(MeasurementUtils.getHumidityMeasurement(rawhumidity));
                if(fitsRefreshRate(MeasurementManager.TYPE_HUMIDITY)) {
                    getMeasurementManager().addHumidity(measurement);
                }
            }
            else if(characteristic.getUuid().equals(TemperatureUUID)){
                byte[] rawtemperature =  characteristic.getValue();
                MeasurementModel measurement = new MeasurementModel(MeasurementUtils.getTemperatureMeasurement(rawtemperature));
                if(fitsRefreshRate(MeasurementManager.TYPE_TEMPERATURE)) {
                    getMeasurementManager().addTemperature(measurement);
                }
            }
            else if(characteristic.getUuid().equals(LightUUID)){
                byte[] rawlight =  characteristic.getValue();
                MeasurementModel measurement = new MeasurementModel(MeasurementUtils.getLightMeasurement(rawlight));
                if(fitsRefreshRate(MeasurementManager.TYPE_LIGHT)) {
                    getMeasurementManager().addLight(measurement);
                }
            }
        }
    };

    /**
     * Scans all the devices around via bluetooth.
     *
     * @param wsdBleScanListener Listener linked to the scan
     */
    public void scanAvailableDevices(WSDBleScanListener wsdBleScanListener) {
        // Initialize BLE Listener
        mBleScanListener = wsdBleScanListener;
        scanListDevices = new ArrayList<>();

        bluetoothLeScanner = BluetoothLeScannerCompat.getScanner();
        final ScanSettings scanSettings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_BALANCED)
                // Refresh the devices list continuously
                .setReportDelay(0)
                // Hardware filtering has some issues on selected devices
                .setUseHardwareBatchingIfSupported(false)
                .build();

        ScanFilter scanFilterDES = new ScanFilter.Builder()
                .setServiceUuid(ParcelUuid.fromString(WSDemoUUID.toString()))
                .build();

        List<ScanFilter> filterList = new ArrayList<>();
        filterList.add(scanFilterDES);

        // We create and define a ScanCallback that is used to capture events during the scan
        scanCallback = new ScanCallback() {

            /**
             * The onScanResult callback is called when a BLE advertisement has been found.
             *
             * @param callbackType Determines how this callback was triggered. Could be one of
             *            {@link ScanSettings#CALLBACK_TYPE_ALL_MATCHES},
             *            {@link ScanSettings#CALLBACK_TYPE_FIRST_MATCH} or
             *            {@link ScanSettings#CALLBACK_TYPE_MATCH_LOST}
             * @param result A Bluetooth LE scan result.
             */
            @Override
            public void onScanResult(final int callbackType, final ScanResult result) {
                if(scanListDevices.size() > 0) {
                    if(!scanListDevices.contains(result.getDevice())){     //Avoid to include repeated devices when scanning
                        scanListDevices.add(new DeviceModel(result));
                        mBleScanListener.onDeviceScanned(new DeviceModel(result));
                    }
                }
                else{
                    scanListDevices.add(new DeviceModel(result));
                    mBleScanListener.onDeviceScanned(new DeviceModel(result));
                }
            }
        };
        bluetoothLeScanner.startScan(filterList, scanSettings, scanCallback);
        mScanning = true;
    }

    /**
     * Method to check if an upcoming data from the sensor has to be discarded because it has been
     * received before the next refreshing time
     *
     * @param sensor_type   The sensor type of the measurement received.
     * @return true if the data should be processed, false otherwise.
     */
    private boolean fitsRefreshRate(int sensor_type){
        long timestampLastMeasurement = MeasurementManager.getInstance().getLatestTimestamp(sensor_type);
        long currentTimestamp = System.currentTimeMillis();

        return (currentTimestamp - timestampLastMeasurement) > refreshTime;
    }


    /**
     * Method to check if there is a device connected
     *
     * @return true if there is a device connected, false otherwise
     */
    public boolean isDeviceConnected() {
        return isDeviceConnected;
    }

    /**
     * Method to generate the sessionID for the current connection
     */
    public void generateSessionID(){
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        for(int i=0; i<SessionIDLength;i++){        // We generate 'SessionIDLength' number of Hex numbers
            stringBuilder.append(Integer.toHexString(random.nextInt(16)));
        }
        sessionID = stringBuilder.toString();
    }

    /**
     * Method that returns the current sessionID
     *
     * @return  SessionID of the current connection
     */
    public String getSessionID(){
        return sessionID;
    }

    /**
     * This method returns the name of the device
     * @return String with the name of the device
     */
    public String getDeviceName (){
        if(mDevice!=null)
            return mDevice.getName();
        else
            return "No device connected";
    }

    /**
     * Returns the device MAC address of the device connected
     * @return MAC address of the device connected. In case there is no device connected it returns
     *         * the MAC structure with 'x'
     */
    public String getDeviceMAC (){
        if(mDevice!=null)
            return mDevice.getAddress();
        else
            return "xxxx:xxxx:xxxx:xxxx";
    }

    /**
     * Returns a DeviceModel object with the information of the RapidIoT device that it is currently
     * connected to.
     * @return DeviceModel with the information of the currently connected device. Returns null if
     *      * there is no device connected.
     */
    public DeviceModel getDevice(){
        return mDevice;
    }

    /**
     * Method to trigger the disconnection from the device it is connected to.
     */
    public void disconnectDevice(){
        disconnect();
        getMeasurementManager().clearMeasurements();
        isDeviceConnected = false;
        mDevice = null;
    }

    /**
     * Method to set the boolean that indicates if a device is connected or not.
     *
     * @param deviceConnected Boolean value to assign.
     */
    public void setDeviceConnected(boolean deviceConnected) {
        isDeviceConnected = deviceConnected;
    }

    // Init BLE connection to the selected bleDevice (first parameter)
    public void initConnection(DeviceModel bleDevice) {
        List<ParcelUuid> serviceUuidList = bleDevice.getServiceUuidList();
        mDevice = bleDevice;
        for(int i=0; i< serviceUuidList.size(); i++){
            if(serviceUuidList.get(i).getUuid().equals(WSDemoUUID)){
                mDevice.setDeviceType("WSDDemoUUID");
            }
        }
        connect(mDevice.getDevice());
    }

    /**
     * Method to stop the scanning process
     */
    public void stopScan(){
        if(mScanning) {
            Log.d(APPTAG, "Stopping scan...");
            mScanning = false;
            bluetoothLeScanner.stopScan(scanCallback);
            if(scanListDevices!=null) {
                Log.d(APPTAG, "scanList Device Not null. Number of devices: " + scanListDevices.size() );
                mBleScanListener.onDeviceScanTimeOut(scanListDevices);
            }
            else
                Log.d("WSDemoTests", "No device found");
            mHandlerScan.removeCallbacks(mRunnableScan);
        }
    }

    // Shortcut to get the Measurement Manager
    private MeasurementManager getMeasurementManager(){
        return MeasurementManager.getInstance();
    }

}
