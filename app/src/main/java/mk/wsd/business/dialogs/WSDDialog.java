package mk.wsd.business.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.icu.util.Measure;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import mk.wsd.R;
import mk.wsd.business.utils.MeasurementManager;

import static android.content.Context.MODE_PRIVATE;

/**
 * Personalized dialog for the Cloud Settings menu.
 */
public class WSDDialog {

    private SharedPreferences mSharedPreferences;
    private TextView tv_clearclouddata;
    private Activity activity;
    private Switch postDataSwitch;

    /**
     * The ShowDialog method is called to show the Cloud Configuration dialog
     * @param activity activity from where the dialog is created
     */
    public void showDialog(Activity activity){
        final Dialog dialog = new Dialog(activity);
        this.activity = activity;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.wsddialog_layout);

        tv_clearclouddata = dialog.findViewById(R.id.clouddialog_textview);
        postDataSwitch = dialog.findViewById(R.id.clouddialog_switch);
        LinearLayout okButton = dialog.findViewById(R.id.llayout_wsddialog_ok);
        okButton.setOnClickListener(v -> dialog.dismiss());

        // The 'postDataSwitch' allows the user to enable the posting data to cloud feature
        postDataSwitch.setOnCheckedChangeListener((compoundButton, isChecked) -> {

            // We prepare the SharedPreferences editor to save the new configuration state
            mSharedPreferences = getSharedPreferences();
            SharedPreferences.Editor editor = mSharedPreferences.edit();


            if(isChecked) {
                // isChecked is true it means that the user has activate postData feature

                // We have to check if we have internet connection
                if(isOnline()){
                    // If we have internet connection we change the SharedPreferences and start the
                    // service to post data
                    editor.putBoolean(activity.getResources().getString(R.string.sharedPref_key_postingdata), true);
                    MeasurementManager.getInstance().startPostDataService(activity);
                }
                else{
                    // If we don't have internet connection we return the switch to its previous
                    // state and show a notification to the user
                    postDataSwitch.setChecked(false);
                    Toast.makeText(activity.getApplicationContext(), activity.getResources().getString(R.string.nointernetconnection_toast_message), Toast.LENGTH_LONG).show();
                }
            }
            else {
                // Checked is false, so we update the SharedPreferences editor and stop the posting service
                editor.putBoolean(activity.getResources().getString(R.string.sharedPref_key_postingdata), false);
                MeasurementManager.getInstance().stopPostDataService(activity);
            }
            editor.apply();     // Apply changes in the editor
        });

        setInitialStates();

        dialog.show();

        // We set an OnClickListener in the clear cloud textview
        tv_clearclouddata.setOnClickListener(v -> {

            //Checking if we have internet connection
            if(isOnline()) {
                // If we have Internet connection we start the background service to clear the data
                // from the current session. Additionally, we change the text color to show the user
                // that the action was processed.

                MeasurementManager.getInstance().startClearDataService(activity);
                tv_clearclouddata.setTextColor(activity.getResources().getColor(R.color.color_nxp_blue));
                Toast.makeText(activity.getApplicationContext(), activity.getResources().getString(R.string.cleardatabase_confirmation_toast_message), Toast.LENGTH_LONG).show();
            }
            else{
                // If we don't have internet connection we make sure that the switch is not checked
                // and notify the user with a toast
                postDataSwitch.setChecked(false);
                Toast.makeText(activity.getApplicationContext(), activity.getResources().getString(R.string.nointernetconnection_toast_message), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Method to initialize the posting data switch. It loads the current configuration from the
     * SharedPreferences and set the switch accordingly
     */
    private void setInitialStates(){
        mSharedPreferences = getSharedPreferences();

        if(mSharedPreferences.getBoolean(activity.getResources().getString(R.string.sharedPref_key_postingdata), false))
            postDataSwitch.setChecked(true);
        else
            postDataSwitch.setChecked(false);
    }

    // Shortcut to get the activity SharedPreferences
    private SharedPreferences getSharedPreferences (){
        return activity.getSharedPreferences(activity.getResources().getString(R.string.sharedPref_name), MODE_PRIVATE);
    }

    /**
     * Method to check that the application/phone has Internet access
     * @return True in case there is internet connection. False otherwise.
     */
    private boolean isOnline(){
        ConnectivityManager cm =
                (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}