package mk.wsd.business.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import mk.wsd.R;
import mk.wsd.business.bluetooth.WSDBleManager;
import mk.wsd.business.datamodel.DeviceModel;

/**
 * The DevicesAdapter helps us handling the list of
 * devices that is used in the Welcome activity
 */
public class DevicesAdapter extends ArrayAdapter<DeviceModel> {

    private Context context;

    public DevicesAdapter(Context context) {
        super(context, R.layout.device_item);
        this.context = context;
    }


    /**
     *  We define here the ViewHolder class with the related layout elements that we will use
     *  later to customize the device view.
     */
    static class ViewHolder {
        TextView tvTitle;
        RelativeLayout rlDevice;

        ViewHolder(View view) {
            tvTitle = view.findViewById(R.id.text_title);
            rlDevice = view.findViewById(R.id.relativelayout_device);
        }
    }


    /**
     * Method to get a View that displays the information about the device related to a specific position
     *
     * @param position The position of the device in the adapter's data set
     * @param convertView The old View to reuse, in case it is possible. We should check if it is non-null
     * @param parent The parent that the current View will eventually be attached to
     * @return A View corresponding to the device at the specified position
     */
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        // We get the View Holder from the old View or we inflate it from the layout device_item view
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.device_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        // We personalize the information for the specified position
        DeviceModel model = getItem(position);
        holder.tvTitle.setText(model.getName());    // Include name

        // Check if there is a device connected
        if(WSDBleManager.getInstance(this.context).isDeviceConnected()){
            // If there is a device connected we should check if it is the one related to this specific View
            DeviceModel currentConnectedDevice = WSDBleManager.getInstance(this.context).getDevice();
            if(model.getAddress().equals(currentConnectedDevice.getAddress())){
                // The device connected is the one linked to this View. We set a highlighted background
                holder.rlDevice.setBackgroundColor(this.context.getResources().getColor(R.color.color_devicelist_highlightedbackground));
            }
            else{
                // Is not the current device, so we set a normal background
                holder.rlDevice.setBackgroundColor(this.context.getResources().getColor(R.color.color_devicelist_normalbackground));
            }
        }
        else{
            // There is no device connected, so all backgrounds will be normal
            holder.rlDevice.setBackgroundColor(this.context.getResources().getColor(R.color.color_devicelist_normalbackground));
        }
        return convertView;
    }
}
