package mk.wsd.business.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mk.wsd.business.datamodel.TutorialModel;

/**
 * Adapter that we will use to populate the instructions pages in the tutorial menu
 */
public class TutorialAdapter extends PagerAdapter {

    private Context adapterContext;

    public TutorialAdapter(Context context) {
        adapterContext = context;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, int position) {
        TutorialModel modelObject = TutorialModel.values()[position];
        LayoutInflater inflater = LayoutInflater.from(adapterContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(modelObject.getLayoutResId(), collection, false);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return TutorialModel.values().length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        TutorialModel customPagerEnum = TutorialModel.values()[position];
        return adapterContext.getString(customPagerEnum.getTitleResId());
    }

}
