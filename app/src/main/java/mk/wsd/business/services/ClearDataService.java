package mk.wsd.business.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import mk.wsd.business.bluetooth.WSDBleManager;
import mk.wsd.business.mqtt.MQManager;
import mk.wsd.business.utils.MeasurementManager;

import static mk.wsd.business.activities.BLEActivity.APPTAG;

/**
 *  Service created to send a message to the backend to Clear the data from a specific sessionID.
 *  The service creates a background thread to handle the task without blocking the UI thread.
 */
public class ClearDataService extends Service {

    private MQManager myMQManager;  // To communicate with the MQTT broker and send the request to Clear Data Service

    final class ServiceThread implements Runnable{
        int serviceId;  // To keep a reference of the ServiceID that created this thread

        ServiceThread(int serviceId){
            this.serviceId = serviceId;
        }

        @Override
        public void run() {
            synchronized (this){
                //We check that the MQManager is connected to send the message
                if(myMQManager.isConnected()){
                    myMQManager.clearDataFromCloud();
                }
            }
            stopSelf();
        }
    }

    public ClearDataService() {}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // We create the instance of the MQManager that we will later use in the thread and connect
        myMQManager = MQManager.getInstance(getApplicationContext());
        myMQManager.connect();

        // We create the thread and start it
        Thread thread = new Thread(new ServiceThread(startId));
        thread.start();

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        // We disconnect the MQTT connection when the service is destroyed
        myMQManager.disconnect();
        super.onDestroy();
    }
}
