package mk.wsd.business.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;
import mk.wsd.R;
import mk.wsd.business.mqtt.MQManager;

import static mk.wsd.business.activities.BLEActivity.APPTAG;

public class PostDataService extends Service {

    private int postingPeriod;      // To keep track of the frequency we want to be posting data to the cloud
    private boolean active;         // To control that the thread stops when the service is finished from outside
    private Thread thread;          // We declare it global so we can interrupt the thread from the BroadcastListener
    private MQManager myMQManager;


    final class ServiceThread implements Runnable{
        int serviceId;  // To keep a reference of the ServiceID that created this thread

        ServiceThread(int serviceId){
            this.serviceId = serviceId;
        }

        @Override
        public void run() {
            synchronized (this){
                while(active) { // this allows us to stop the thread when the service is stopped

                    //We check that the MQManager is connected to publish the latest set of measurements
                    if(myMQManager.isConnected()){
                        myMQManager.publishLastMeasurement();
                    }else{
                        //The connection might be lost. Reconnecting
                        myMQManager.connect();
                    }

                    try{
                        wait(postingPeriod);    // to sleep the thread until it comes the time to send the next data
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public PostDataService() {}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // We create the instance of the MQManager that we will use later in the thread
        myMQManager = MQManager.getInstance(getApplicationContext());
        myMQManager.connect();

        // We create the thread and start it
        thread = new Thread(new ServiceThread(startId));
        thread.start();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        active = true;

        // When we create the service we read the current posting frequency
        updatePostingPeriod();

        // We register the Broadcast Receiver to get posting frequency updates
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter(getResources().getString(R.string.broadcastreceiver_change_refreshrate)));
    }

    @Override
    public void onDestroy() {
        active = false;     // To close the thread linked
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);   // Unregister the receiver
        myMQManager.disconnect();   // Disconnect the connection to the MQTT broker
        super.onDestroy();
    }

    /**
     * BroadcastReceiver created to received a notification when the posting period configuration
     * is modified by the user in the Settings screen.
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updatePostingPeriod();  // We update the postingPeriod
            thread.interrupt();
        }
    };

    /**
     * Method to update the posting period by reading the value from the
     * Shared Preferences of the application
     */
    private void updatePostingPeriod (){
        int defRefreshRateValue = getResources().getInteger(R.integer.defaultRefreshRateInMillis);  // Default value needed when reading from SharedPreferences
        SharedPreferences mSharedPreferences = getSharedPreferences(getResources().getString(R.string.sharedPref_name), MODE_PRIVATE);
        postingPeriod = mSharedPreferences.getInt(getResources().getString(R.string.sharedPref_key_refreshrate), defRefreshRateValue);
        Log.d(APPTAG, "Posting period set to: " + postingPeriod);

    }
}
