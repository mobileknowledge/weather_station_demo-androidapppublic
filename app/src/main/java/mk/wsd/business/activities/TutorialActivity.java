package mk.wsd.business.activities;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import mk.wsd.R;
import mk.wsd.business.adapters.TutorialAdapter;
import mk.wsd.business.utils.ViewPagerIndicator;

public class TutorialActivity extends AppCompatActivity {

    private static final int TUTORIAL_PAGES = 5;
    private ViewPager viewPager;
    private Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tutorial);

        viewPager = findViewById(R.id.intro_viewpager);         // We use a ViewPager to display the slide deck
        nextButton = findViewById(R.id.button_tutorial_next);

        if (!(viewPager.getCurrentItem() == 0))
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);

        nextButton.setOnClickListener(view -> {       // 'Next' button actions:
            if (viewPager.getCurrentItem() == (TUTORIAL_PAGES-1)) {     // If it's the last page we jump to Welcome screen
                startActivity(new Intent(TutorialActivity.this, WelcomeActivity.class));
                finish();
            }
            else {             // If it is not the last page we jump to the next one
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                if (viewPager.getCurrentItem() == (TUTORIAL_PAGES-1)) {                 //If the current page is the last
                    findViewById(R.id.textview_tutorial_skip).setVisibility(View.GONE); //one, we disable the skip option
                }
            }
        });

        findViewById(R.id.textview_tutorial_skip).setOnClickListener(v -> {                          // We set the an option to skip the tutorial
            startActivity(new Intent(TutorialActivity.this, WelcomeActivity.class));    //and move back to Welcome activity
            finish();
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {    //Listener for the scrolling option

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {      // When we scroll to another page we check if it is the
                if (position == (TUTORIAL_PAGES-1)) {       // last one to disable the skip option
                    findViewById(R.id.textview_tutorial_skip).setVisibility(View.GONE);
                    nextButton.setText(R.string.button_tutorial_close);
                } else {
                    findViewById(R.id.textview_tutorial_skip).setVisibility(View.VISIBLE);
                    nextButton.setText(R.string.button_tutorial_next);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        ViewPagerIndicator indicator = findViewById(R.id.introduction_indicator);

        viewPager.setAdapter(new TutorialAdapter(this));
        indicator.setViewPager(viewPager);
    }

}
