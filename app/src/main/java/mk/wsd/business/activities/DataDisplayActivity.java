package mk.wsd.business.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;
import mk.wsd.R;
import mk.wsd.business.bluetooth.WSDBleManager;
import mk.wsd.business.datamodel.MeasurementModel;
import mk.wsd.business.dialogs.WSDDialog;
import mk.wsd.business.utils.MeasurementManager;

public class DataDisplayActivity extends BLEActivity {

    private TextView textViewMeasurement1, textViewMeasurement2, textViewMeasurement3, textViewMeasurement4, textViewSessionID;
    private TextView textViewMeasurementTime1, textViewMeasurementTime2, textViewMeasurementTime3, textViewMeasurementTime4;
    private int refreshRate;
    private boolean temperatureUnitCelsius = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(APPTAG, "onCreate - DataDisplay Activity");

        setContentView(R.layout.activity_data_display);

        // Main UI elements instantiation
        TextView connectionStatusTV = findViewById(R.id.textview_ddisplay_connectedTo);
        LinearLayout linearLayoutTemperature = findViewById(R.id.llayout_ddisplay_measurementTypeTemperature);
        LinearLayout linearLayoutHumidity = findViewById(R.id.llayout_ddisplay_measurementTypeHumidity);
        LinearLayout linearLayoutPressure = findViewById(R.id.llayout_ddisplay_measurementTypePressure);
        LinearLayout linearLayoutLight = findViewById(R.id.llayout_ddisplay_measurementTypeLight);
        Button buttonCloudSettings = findViewById(R.id.button_datadisplay_cloudsettings);

        textViewSessionID = findViewById(R.id.textview_ddisplay_sessionID);
        textViewMeasurement1 = findViewById(R.id.textview_ddisplay_measurement1);
        textViewMeasurement2 = findViewById(R.id.textview_ddisplay_measurement2);
        textViewMeasurement3 = findViewById(R.id.textview_ddisplay_measurement3);
        textViewMeasurement4 = findViewById(R.id.textview_ddisplay_measurement4);
        textViewMeasurementTime1 = findViewById(R.id.textview_ddisplay_measurementtimestamp1);
        textViewMeasurementTime2 = findViewById(R.id.textview_ddisplay_measurementtimestamp2);
        textViewMeasurementTime3 = findViewById(R.id.textview_ddisplay_measurementtimestamp3);
        textViewMeasurementTime4 = findViewById(R.id.textview_ddisplay_measurementtimestamp4);

        connectionStatusTV.setText(getResources().getString(R.string.all_connectedTo, WSDBleManager.getInstance(getApplicationContext()).getDeviceName()));

        // The LinearLayouts containing the Measurement types act as a button to change the type of
        // measurement that is plotted in the table.
        // Here we set the listeners for every layout. In every listener we start by changing the
        // selected measurement type and then update the table
        linearLayoutTemperature.setOnClickListener(view -> {
            MeasurementManager.getInstance().setSelectedType(MeasurementManager.TYPE_TEMPERATURE);
            updateTable();
        });
        linearLayoutHumidity.setOnClickListener(view -> {
            MeasurementManager.getInstance().setSelectedType(MeasurementManager.TYPE_HUMIDITY);
            updateTable();
        });
        linearLayoutPressure.setOnClickListener(view -> {
            MeasurementManager.getInstance().setSelectedType(MeasurementManager.TYPE_PRESSURE);
            updateTable();
        });
        linearLayoutLight.setOnClickListener(view -> {
            MeasurementManager.getInstance().setSelectedType(MeasurementManager.TYPE_LIGHT);
            updateTable();
        });

        // The 'Cloud Settings' button opens a dialog with the configuration for the Cloud posting feature
        buttonCloudSettings.setOnClickListener(view -> {
            WSDDialog dialog = new WSDDialog();
            dialog.showDialog(DataDisplayActivity.this);
        });

        // Load the current value of the refresh rate from the SharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences(getResources().getString(R.string.sharedPref_name), MODE_PRIVATE);
        int defaultRefreshRateValue = getResources().getInteger(R.integer.defaultRefreshRateInMillis);
        refreshRate = sharedPreferences.getInt(getResources().getString(R.string.sharedPref_key_refreshrate), defaultRefreshRateValue);
        Log.d(APPTAG, "The refresh rate is now: " + refreshRate);
        getBLEManager().updateRefreshRate();

        // Load the current value of the temperature unit from the SharedPreferences
        String defaultTemperatureConfig = getResources().getString(R.string.temperatureConfigDefault);
        String temperatureConfig = sharedPreferences.getString(getResources().getString(R.string.sharedPref_key_temperature), defaultTemperatureConfig);
        temperatureUnitCelsius = temperatureConfig.equals(getResources().getString(R.string.temperatureConfigDefault));

        //Start the thread that will run in background to periodically update the UI
        updateUIThread.start();
    }



    @Override
    protected void onResume() {
        super.onResume();
        setToolbarText(getString(R.string.ddisplay_actionbar_title));

        // We update the BLEManagerCallbacks
        getBLEManager().setGattCallbacks(this);

        //Everytime the activity is resumed we update the table
        updateTable();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goBackToWelcomeScreen();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        updateUIThread.interrupt(); // We stop the update UI thread
    }

    /**
     * The updateTable method is called to initialize the configuration of the table. In this method
     * we check the type of measurement that is selected and highlight its tab in relation to the others.
     * This method is separated from the updateData for optimization as the updateData will be called
     * more often and we don't need to update the table tab and colors every time a measurement arrives.
     */
    private void updateTable (){
        //We load the measurement type from the Measurement Manager
        int measurementType = MeasurementManager.getInstance().getSelectedType();

        LinearLayout tvTemperature = findViewById(R.id.llayout_ddisplay_measurementTypeTemperature);
        LinearLayout tvHumidity = findViewById(R.id.llayout_ddisplay_measurementTypeHumidity);
        LinearLayout tvPressure = findViewById(R.id.llayout_ddisplay_measurementTypePressure);
        LinearLayout tvLight = findViewById(R.id.llayout_ddisplay_measurementTypeLight);

        // We set the sessionID field with the SessionID generated for the current connection
        textViewSessionID.setText(getResources().getString(R.string.ddisplay_sessionID_prefix, getBLEManager().getSessionID()));

        // Initially we set all the tabs to 'unfocus' state
        tvTemperature.setBackgroundResource(R.drawable.ddisplay_table_field);
        tvHumidity.setBackgroundResource(R.drawable.ddisplay_table_field);
        tvPressure.setBackgroundResource(R.drawable.ddisplay_table_field);
        tvLight.setBackgroundResource(R.drawable.ddisplay_table_field);

        // Depending on the measurement type we highlight the tab of that particular tab
        switch (measurementType){
            case (MeasurementManager.TYPE_TEMPERATURE):
                tvTemperature.setBackgroundResource(R.drawable.ddisplay_table_field_highlighted);
                break;
            case (MeasurementManager.TYPE_HUMIDITY):
                tvHumidity.setBackgroundResource(R.drawable.ddisplay_table_field_highlighted);
                break;
            case (MeasurementManager.TYPE_PRESSURE):
                tvPressure.setBackgroundResource(R.drawable.ddisplay_table_field_highlighted);
                break;
            case (MeasurementManager.TYPE_LIGHT):
                tvLight.setBackgroundResource(R.drawable.ddisplay_table_field_highlighted);
                break;
            default:
                break;
        }

        // And then we call the updateData method to update
        // the measurement values displayed on the table
        updateData();
    }

    /**
     * The updateData method updates the measurement values that are displayed in the table. This
     * method is called with a periodicity defined by the refresh rate.
     */
    @SuppressLint("SetTextI18n")
    private void updateData(){
        // We create a string and get the unit to be used from the getUnit method from the Measurement Manager
        // we pass a boolean to define if we should get Celsius or Fahrenheit, only in case the temperature is selected
        String unit = MeasurementManager.getInstance().getUnit(temperatureUnitCelsius);

        // Using the MeasurementManager we obtain the current measurement type selected and
        // an array with the latest set of measurements received
        int measurementType = MeasurementManager.getInstance().getSelectedType();
        ArrayList<MeasurementModel> arrayUpdateMeasurement = MeasurementManager.getInstance().getSelectedMeasurementArray();

        // We initialize all text to "--"
        textViewMeasurement1.setText("--");
        textViewMeasurement2.setText("--");
        textViewMeasurement3.setText("--");
        textViewMeasurement4.setText("--");

        float multiplyFactor = 1;       // Factors that we apply to the float stored. It will only affect when ºF is configured
        float sumOffset = 0;            // and Temperature is plotted in table. Otherwise, it won't be altered -> multiply by 1, sum + 0

        if((!temperatureUnitCelsius)&&(measurementType == MeasurementManager.TYPE_TEMPERATURE)){
            // If temperature is selected and the unit is Fahrenheit we apply the following correction factor:
            multiplyFactor = (float) 1.8;
            sumOffset = (float) 32;
        }

        // Depending on the number of elements that we have in the measurement lists we update the text fields from
        // the table. If the size if 4 we will assign from the lowest field to the upper one (4, 3, 2, 1). If the
        // size is 3 we change from the second starting from the bottom and then up (3, 2, 1) and so on...
        switch (arrayUpdateMeasurement.size()) {
            case (4):
                textViewMeasurement4.setText((arrayUpdateMeasurement.get(arrayUpdateMeasurement.size() - 4).getValue()*multiplyFactor+sumOffset) + unit);
                textViewMeasurementTime4.setText((arrayUpdateMeasurement.get(arrayUpdateMeasurement.size() - 4).getTimeStamp()));
            case (3):
                textViewMeasurement3.setText((arrayUpdateMeasurement.get(arrayUpdateMeasurement.size() - 3).getValue()*multiplyFactor+sumOffset) + unit);
                textViewMeasurementTime3.setText((arrayUpdateMeasurement.get(arrayUpdateMeasurement.size() - 3).getTimeStamp()));
            case (2):
                textViewMeasurement2.setText((arrayUpdateMeasurement.get(arrayUpdateMeasurement.size() - 2).getValue()*multiplyFactor+sumOffset) + unit);
                textViewMeasurementTime2.setText((arrayUpdateMeasurement.get(arrayUpdateMeasurement.size() - 2).getTimeStamp()));
            case (1):
                textViewMeasurement1.setText((arrayUpdateMeasurement.get(arrayUpdateMeasurement.size() - 1).getValue()*multiplyFactor+sumOffset) + unit);
                textViewMeasurementTime1.setText((arrayUpdateMeasurement.get(arrayUpdateMeasurement.size() - 1).getTimeStamp()));
            default:
                break;
        }
    }

    /**
     * Thread that will run in background to update the UI elements with a certain refresh rate.
     */
    Thread updateUIThread = new Thread() {
        @Override
        public void run() {
            try {
                while (!updateUIThread.isInterrupted()) {
                    runOnUiThread(() -> updateData());
                    Thread.sleep(refreshRate);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    };

    //------------ Methods for redirecting to other activities -----------//
    private void goBackToWelcomeScreen (){
        Intent backToWelcomeIntent = new Intent(this, WelcomeActivity.class);
        startActivity(backToWelcomeIntent);
        finish();
    }

}
