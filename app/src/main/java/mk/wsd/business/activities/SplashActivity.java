package mk.wsd.business.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * The splash activity is the launcher activity.
 * It is shown at application start to display the application title and logo
 */
public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_DURATION_MILLIS = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        timer.start();  //Start the timer
    }

    /**
     * Background timer thread (non-blockable) used to show the Splash Screen for a certain period
     * and later move to Welcome Activity
     */
    Thread timer=new Thread()
    {
        public void run() {
            try {
                sleep(SPLASH_DURATION_MILLIS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            finally
            {
                Intent i=new Intent(SplashActivity.this, WelcomeActivity.class);
                finish();
                startActivity(i);
            }
        }
    };
}
