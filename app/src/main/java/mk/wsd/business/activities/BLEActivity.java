package mk.wsd.business.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;
import mk.wsd.R;
import mk.wsd.business.bluetooth.WSDBleManager;
import mk.wsd.business.utils.BLEUtils;
import no.nordicsemi.android.ble.BleManagerCallbacks;

import static android.os.Build.VERSION_CODES.M;

/**
 * The BLEActivity is the parent activity for all the activities in the application where a BLE
 * connection can be triggered or handle. It exposes methods to check the BLE/GPS adapter, register
 * or unregister receivers.
 * It also implements the BleManagerCallbacks methods, to control the active BLE channel
 */
@SuppressLint("Registered")
public class BLEActivity extends AppCompatActivity implements BleManagerCallbacks {

    public final static String APPTAG= "WSDemo-DEV";

    LocationManager locationManager;
    protected boolean skipBLEActivationCheck = false;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 100;
    static final int MSG_DISMISS_DIALOG = 0;
    static final int CONNECTING_TIMEOUT = 15000;
    protected ProgressDialog connectingDialog;
    protected boolean bondingRequired = false;
    protected Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     *  Method to check if the Bluetooth is active. In case the bluetooth adapter is not enabled
     *  the application will redirect the user to the activation menu.
     *  NOTE: For Android versions equal or above 23, the GPS should be turned on in order to get
     *  BLE access. For these versions, the application also check and require the user to activate
     *  it.
     */
    protected void checkIfBluetoothIsActive(){
        Log.d(APPTAG, "Checking BLE");
        if (Build.VERSION.SDK_INT >= M) {
            //For versions 23+ we need to check that GPS is enabled
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                // GPS is disabled. Displaying dialog to the user
                buildAlertMessageNoGps();
            }
            else{
                //GPS is turned on, now we need to check the permissions
                if(checkLocationPermission()) {
                    //Application has permissions, we need to check now the BLE adapter...
                    checkAndActivateBLEAdapter();
                }
            }
        }
        else{
            //We don't need to check GPS, only BLE
            checkAndActivateBLEAdapter();
        }
    }

    private void checkAndActivateBLEAdapter(){
        if(!BLEUtils.isBluetoothEnabled()){
            // Showing dialog requiring the user to enable the Bluetooth connection
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, BLEUtils.REQUEST_ENABLE_BT);
        }
        else{
            //Bluetooth is enabled
            Log.d(APPTAG, "Bluetooth already enabled");
            onBLEEnabled();
        }
    }

    /**
     * Method to check if the application has permissions to access location
     * @return true in case it has permissions, false otherwise.
     */
    private boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(R.string.requestpermission_location_title)
                        .setMessage(R.string.requestpermission_location_message)
                        .setPositiveButton(R.string.requestpermission_location_positivebutton, (dialogInterface, i) -> {
                            //Prompt the user once explanation has been shown
                            ActivityCompat.requestPermissions(BLEActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    MY_PERMISSIONS_REQUEST_LOCATION);
                        })
                        .create()
                        .show();
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * Method to catch the result of the request permission for the Location access
     *
     * @param requestCode Request code that was passed in the call to the requestPermissions
     * @param permissions The requested permissions
     * @param grantResults Result for the requested permissions (PERMISSION_GRATED or PERMISSION_DENIED)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        //String provider = locationManager.getBestProvider(new Criteria(), false);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // Permission denied. As shown to the user, the application closes.
                    finish();
                }

                // If Location permission was granted no action is needed as the activity is
                // resumed and it will check the BLE automatically.
            }
        }
    }

    /**
     * Method that is called after the user has chosen to enable or not the BLE permissions for the
     * application.
     *
     * @param requestCode Request code that was passed in the call to the startActivityForResult
     * @param resultCode To see if the request was accepted
     * @param data Additional data wrapped in an intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == BLEUtils.REQUEST_ENABLE_BT){
            if(resultCode!=RESULT_OK){
                // If the user declined to activate bluetooth a dialog is shown to warn the user
                // that BLE is needed for the application and it will close
                Log.d(APPTAG, "Bluetooth was not activated");
                final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                builder.setTitle(R.string.alertdialog_messagebleoffclosing_title)
                        .setMessage(R.string.alertdialog_messagebleoffclosing_message)
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.ok, null)
                        .setPositiveButton(R.string.alertdialog_messagebleoffclosing_positivebutton, (dialog, id) -> finish());
                builder.show();
                skipBLEActivationCheck = true;  // We will skip the regular BLE check as the application will close
            }
            else{
                //Bluetooth was activated by the user
                Log.d(APPTAG, "Bluetooth has been activated");
                onBLEEnabled();
            }
        }
    }

    /**
     * Method called when the BLE connection is checked. As a normal procedure, we will check that we
     * have internet access. This method is 'protected' so child activities can overwrite and
     * implement actions accordingly.
     */
    protected void onBLEEnabled(){
        checkNetworkConnectivity();
    }

    /**
     * Method to check that the application has internet access. In case not, the user will be
     * notified that Cloud features would be disabled.
     */
    private void checkNetworkConnectivity(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if(!(netInfo != null && netInfo.isConnectedOrConnecting())){
            new AlertDialog.Builder(this)
                    .setTitle(R.string.nointernetconnection_title)
                    .setMessage(R.string.nointernetconnection_message)
                    .setPositiveButton(R.string.nointernetconnection_positivebutton, (dialogInterface, i) -> {
                        //Prompt the user once explanation has been shown
                        dialogInterface.dismiss();
                    })
                    .create()
                    .show();
        }
    }

    protected WSDBleManager getBLEManager(){
        return WSDBleManager.getInstance(getApplicationContext());
    }


    /**
     * A broadcast receiver is used to captures any change in the status of the Bluetooth adapter
     * while the application is opened (e.g. if the user disables the Bluetooth from the notification bar)
     */
    private final BroadcastReceiver mBLEReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);

                if(state==BluetoothAdapter.STATE_OFF){
                    checkIfBluetoothIsActive();
                }
            }
        }
    };

    /**
     * Method that registers the BLE state change receiver.
     */
    protected void registerBLEReceiver(){
        Log.d(APPTAG, "Registering StateChange Receiver...");
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mBLEReceiver, filter);
    }

    /**
     * Method that unregisters the BLE state change receiver.
     */
    protected void unregisterBLEReceiver(){
        Log.d(APPTAG, "Unregistering StateChange Receiver...");
        unregisterReceiver(mBLEReceiver);
    }

    // Method to prompt a message to the user, notifying about the need to have GPS enabled to
    // get Bluetooth communication
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.alertdialog_messagenogps_message)
                .setCancelable(false)
                .setPositiveButton(R.string.alertdialog_messagenogps_positivebutton, (dialog, id) -> startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton(R.string.alertdialog_messagenogps_negativebutton, (dialog, id) -> finish());
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerBLEReceiver();  // We register the BLE receiver every time the activity is resumed

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back_wsd));
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }

    /**
     * Method to set the toolbar title. Every child activity from the BLEActivity defined here,
     * can make use of this method.
     * @param title String with the name that will be displayed in the toolbar
     */
    protected void setToolbarText(String title){
        toolbar.setTitle(title);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterBLEReceiver();  // We unregister the BLE receiver when the activity is finished
    }



    //-------------- Connecting dialog -----------------//

    /**
     * Method to open a dialog while the connection with the RapidIoT device is taking place. It is
     * linked to a handler to link a timer and identify a possible problem with the connection
     */
    protected void showConnectingDialog(){
        connectingDialog = new ProgressDialog(this);
        connectingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        connectingDialog.setMessage("Connecting...");
        connectingDialog.setIndeterminate(true);
        connectingDialog.setCanceledOnTouchOutside(false);
        connectingDialog.show();
        mHandler.sendEmptyMessageDelayed(MSG_DISMISS_DIALOG, CONNECTING_TIMEOUT);
    }

    // Handler used to dismiss the dialog if there is a problem connecting
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MSG_DISMISS_DIALOG:
                    if (connectingDialog != null && connectingDialog.isShowing()) {
                        connectingDialog.dismiss();
                        if(!bondingRequired)
                            Toast.makeText(BLEActivity.this, "Connection timeout reached. Please try again", Toast.LENGTH_LONG).show();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * Method to close the connecting dialog in case an error ocurred.
     */
    protected void forceClosingConnectingDialogDueToError(){
        if (connectingDialog != null){
            if(connectingDialog.isShowing()){
                Toast.makeText(connectingDialog.getContext(), "An error ocurred in the connection....", Toast.LENGTH_LONG).show();
                connectingDialog.dismiss();
            }
        }
    }


    //-------------- BleManagerCallbacks methods ---------------//
    /*
     * Callback methods of the BLE Manager.
    */


    @Override
    public void onDeviceConnecting(BluetoothDevice device) {
        Log.d(APPTAG, "onDeviceConnecting: " + device.getName());
    }

    @Override
    public void onDeviceConnected(BluetoothDevice device) {
        Log.d(APPTAG, "onDeviceConnected: " + device.getName());
    }

    @Override
    public void onDeviceDisconnecting(BluetoothDevice device) {
        Log.d(APPTAG, "onDeviceDisconnecting: " + device.getName());
    }

    @Override
    public void onDeviceDisconnected(BluetoothDevice device) {
        Log.d(APPTAG, "onDeviceDisconnected: " + device.getName());

        if(!(this instanceof WelcomeActivity)){
            // The device was disconnected and we are not in the WelcomeActivity
            // We need to redirect the user to the Welcome screen
            runOnUiThread(() -> new AlertDialog.Builder(BLEActivity.this)
                    .setTitle("Connection lost")
                    .setMessage("The connection with the device was lost. You will be redirected to the Scanning screen")
                    .setPositiveButton(R.string.nointernetconnection_positivebutton, (dialogInterface, i) -> {
                        //Prompt the user once explanation has been shown
                        Intent backToWelcomeIntent = new Intent(BLEActivity.this, WelcomeActivity.class);
                        startActivity(backToWelcomeIntent);
                        finish();
                        dialogInterface.dismiss();
                    })
                    .create()
                    .show());
        }
    }

    @Override
    public void onLinklossOccur(BluetoothDevice device) {
        Log.d(APPTAG, "onLinklossOccur: " + device.getName());
    }

    @Override
    public void onServicesDiscovered(BluetoothDevice device, boolean optionalServicesFound) {
        Log.d(APPTAG, "onServicesDiscovered: " + device.getName());
    }

    @Override
    public void onDeviceReady(BluetoothDevice device) {
        Log.d(APPTAG, "onDeviceReady: " + device.getName());
        WSDBleManager.getInstance(getApplicationContext()).setDeviceConnected(true);
    }

    @Override
    public boolean shouldEnableBatteryLevelNotifications(BluetoothDevice device) {
        Log.d(APPTAG, "shouldEnableBatteryLevelNotifications: " + device.getName());
        return false;
    }

    @Override
    public void onBatteryValueReceived(BluetoothDevice device, int value) {
        Log.d(APPTAG, "onBatteryValueReceived: " + device.getName());
    }

    @Override
    public void onBondingRequired(BluetoothDevice device) {
        Log.d(APPTAG, "onBondingRequired: " + device.getName());
    }

    @Override
    public void onBonded(BluetoothDevice device) {
        Log.d(APPTAG, "onBonded: " + device.getName());
    }

    @Override
    public void onError(BluetoothDevice device, String message, int errorCode) {
        Log.d(APPTAG, "onError: " + device.getName() + " Error: " + message);
        //There was an error connecting, we should close the dialog in case it is still shown
        forceClosingConnectingDialogDueToError();
    }

    @Override
    public void onDeviceNotSupported(BluetoothDevice device) {
        Log.d(APPTAG, "onDeviceNotSupported: " + device.getName());
    }

}
