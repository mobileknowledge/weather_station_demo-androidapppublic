package mk.wsd.business.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import mk.wsd.R;
import mk.wsd.business.bluetooth.WSDBleManager;
import mk.wsd.business.utils.MeasurementManager;

public class SettingsActivity extends BLEActivity {

    private TextView macAddresTextview, macAddresTitleTextview;
    private Switch refreshrateSwitch, temperatureSwitch;
    private ImageView deviceImageView;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        TextView appVersionTextview = findViewById(R.id.textview_settings_appversion_version);
        macAddresTextview = findViewById(R.id.textview_settings_macvalue);
        macAddresTitleTextview = findViewById(R.id.textview_settings_macaddresstitle);
        deviceImageView = findViewById(R.id.imageview_device);
        refreshrateSwitch = findViewById(R.id.switch_settings_refreshrate);
        temperatureSwitch = findViewById(R.id.switch_settings_temperatureconfig);

        // Update the information fields with the application version and device MAC address
        appVersionTextview.setText(getVersion());
        macAddresTextview.setText(getBLEManager().getDeviceMAC());

        // If the RefreshRate switch is clicked we have to update the SharedPreferences with the new value
        refreshrateSwitch.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            mSharedPreferences = getSharedPreferences();
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            if(isChecked)
                editor.putInt(getResources().getString(R.string.sharedPref_key_refreshrate), getResources().getInteger(R.integer.refreshRateOptionRight));
            else
                editor.putInt(getResources().getString(R.string.sharedPref_key_refreshrate), getResources().getInteger(R.integer.refreshRateOptionLeft));
            editor.apply();
            getBLEManager().updateRefreshRate();    // Update the refresh rate from the BLEManager

            // If the background posting service is running we have to notify it about the change using a BroadcastManager
            if(MeasurementManager.getInstance().isPostingServiceRunning(getApplicationContext())) {
                Intent intent = new Intent(getResources().getString(R.string.broadcastreceiver_change_refreshrate));
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                Log.d(APPTAG, "Notifying about change in the refreshrate");
            }
        });

        // If the temperature unit switch is clicked we update the SharedPreferences with the new value
        temperatureSwitch.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            mSharedPreferences = getSharedPreferences();
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            if(isChecked)
                editor.putString(getResources().getString(R.string.sharedPref_key_temperature), getResources().getString(R.string.temperatureConfigCelsius));
            else
                editor.putString(getResources().getString(R.string.sharedPref_key_temperature), getResources().getString(R.string.temperatureConfigFahrenheit));
            editor.apply();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //We personalize the toolbar for the current activity
        setToolbarText(getString(R.string.settings_actionbar_title));

        // We update the BLEManagerCallbacks with the one we define for the current activity
        getBLEManager().setGattCallbacks(this);

        // And we set the initial state for the different UI elements
        setInitialStates();
    }

    /**
     *  Method to initialize the main elements depending on the current configuration. The initial
     *  position of the switches and the visibility of the connected-related fields is checked.
     */
    private void setInitialStates(){
        mSharedPreferences = getSharedPreferences();

        int defRefreshRateValue = getResources().getInteger(R.integer.defaultRefreshRateInMillis);
        String defTemperatureConfigValue = getResources().getString(R.string.temperatureConfigDefault);

        // If there is no device connected we have to set the MAC address
        // and device-related elements to invisible
        if(!WSDBleManager.getInstance(getApplicationContext()).isDeviceConnected()) {
            macAddresTitleTextview.setVisibility(View.INVISIBLE);
            macAddresTextview.setVisibility(View.INVISIBLE);
            deviceImageView.setVisibility(View.INVISIBLE);
        }else{
            macAddresTitleTextview.setVisibility(View.VISIBLE);
            macAddresTextview.setVisibility(View.VISIBLE);
            deviceImageView.setVisibility(View.VISIBLE);
        }

        if(mSharedPreferences.getInt(getResources().getString(R.string.sharedPref_key_refreshrate), defRefreshRateValue) == defRefreshRateValue)
            refreshrateSwitch.setChecked(true);
        else
            refreshrateSwitch.setChecked(false);

        if(mSharedPreferences.getString(getResources().getString(R.string.sharedPref_key_temperature), defTemperatureConfigValue).equals(defTemperatureConfigValue))
            temperatureSwitch.setChecked(true);
        else
            temperatureSwitch.setChecked(false);

    }

    /**
     * Method to retrieve the application version
     * @return String with the version
     */
    private String getVersion (){
        try {
            PackageInfo pInfo = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "NaN";
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        //If back button is pressed we have to close activity and move to Welcome activity
        Intent welcomeIntent = new Intent(this, WelcomeActivity.class);
        finish();
        startActivity(welcomeIntent);
    }

    // Shortcut method to retrieve the SharedPreferences for the application
    private SharedPreferences getSharedPreferences (){
        return this.getSharedPreferences(getResources().getString(R.string.sharedPref_name), MODE_PRIVATE);
    }

}
