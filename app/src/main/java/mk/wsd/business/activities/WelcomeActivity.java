package mk.wsd.business.activities;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import mk.wsd.R;
import mk.wsd.business.adapters.DevicesAdapter;
import mk.wsd.business.bluetooth.WSDBleManager;
import mk.wsd.business.bluetooth.WSDBleScanListener;
import mk.wsd.business.datamodel.DeviceModel;
import mk.wsd.business.utils.BLEUtils;
import mk.wsd.business.utils.MeasurementManager;

/**
 * The WelcomeActivity is the main activity of the application. It shows the RapidIoT devices that
 * the phone has in range. You can connect to one of the devices by clicking the device on the list.
 */
public class WelcomeActivity extends BLEActivity {

    private ArrayList<DeviceModel> mArrayBLEDevices;    // To keep track of the devices in range that are listed to the user
    private DevicesAdapter adapter;     // Adapter to manage the ListView with the devices scanned
    private boolean shouldCloseConnection = true;   // To check whether we should close the connection when we leave the activity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Button settingsButton = findViewById(R.id.button_welcome_settings);
        Button tutorialButton = findViewById(R.id.button_welcome_tutorial);
        ListView lvDevices = findViewById(R.id.list_devices);

        adapter = new DevicesAdapter(this); // We create an instance of the DevicesAdapter and link
        lvDevices.setAdapter(adapter);             // it to the ListView showing the devices in range

        getBLEManager().setGattCallbacks(this);    // We update the BLEManagerCallbacks

        // We define here a listener for the case when the user clicks in a device from the list
        lvDevices.setOnItemClickListener((adapterView, view, position, id) -> {
            DeviceModel deviceClicked = (DeviceModel) adapterView.getItemAtPosition(position);

            // We checked if we are connected to any device
            if(getBLEManager().isDeviceConnected()){
                Log.d(APPTAG, "There is a device connected");
                // If we are connected, we have to check if the device clicked is the one we are
                // connected to.
                DeviceModel currentConnectedDevice = getBLEManager().getDevice();
                if(deviceClicked.getAddress().equals(currentConnectedDevice.getAddress())){
                    // If the device clicked is the same that we were already connected we move
                    // directly to the DataDisplay screen.
                    goToDataDisplayActivity();
                }
                else{
                    // If the device clicked is different we have to trigger the disconnection to the
                    // current device and connect to the clicked one.
                    showConnectToAnotherDeviceDialog(deviceClicked);
                }
            }
            else {
                // If we are not connected to any device we connect to the device clicked
                Log.d(APPTAG, "No device connected. Connecting...");
                showConnectingDialog();
                getBLEManager().initConnection(deviceClicked);
                getBLEManager().stopScan();
            }
        });

        // If the user clicks and hold on a device from the list we proceed to disconnect from it
        lvDevices.setOnItemLongClickListener((adapterView, view, position, id) -> {
            WSDBleManager mBleManager = getBLEManager();
            DeviceModel deviceClicked = (DeviceModel) adapterView.getItemAtPosition(position);
            DeviceModel currentConnectedDevice = mBleManager.getDevice();

            //We should check if there is a device connected and if that device is the one clicked
            if(mBleManager.isDeviceConnected()) {
                if(deviceClicked.getAddress().equals(currentConnectedDevice.getAddress())){
                    // If the device selected is the one we are connected we show a confirmation dialog
                    showDisconnectDialog();
                    return true;    // As the action was processed we return true to avoid calling the 'single' onClick method afterwards
                }
                else{
                    return false;
                }
            }
            else {
                return false;
            }
        });

        // Move to settings screen
        settingsButton.setOnClickListener(view -> goToSettingsActivity());

        // Move to the tutorial screen
        tutorialButton.setOnClickListener(view -> goToTutorialActivity());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!skipBLEActivationCheck)          // We check if we need to skip the BLE check
            checkIfBluetoothIsActive();

        //We personalize the toolbar for the current activity
        setToolbarText(getString(R.string.welcome_actionbar_title));
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back_transparent));
        toolbar.setNavigationOnClickListener(null);

        updateConnectedToInfo();
    }

    @Override
    protected void onPause() {
        super.onPause();

        //We stop scanning for devices if the user leaves the activity
        WSDBleManager.getInstance(getApplicationContext()).stopScan();
    }

    @Override
    protected void onDestroy() {
        //We stop scanning for devices if we close the app or move to a different screen
        WSDBleManager.getInstance(getApplicationContext()).stopScan();

        if(shouldCloseConnection)   // Check if we need to close connection (leaving app) or keep
            closeConnection();      // the connection opened (moving to a different activity)

        super.onDestroy();
    }

    @Override
    protected void onBLEEnabled() {
        super.onBLEEnabled();

        // In the Welcome activity once we have checked that the BLE is enabled we have to start scanning
        scanForBLEdevices();
    }

    /**
     * Method to update the UI with information about the device we are connected to
     */
    private void updateConnectedToInfo(){
        TextView connectedTextView = findViewById(R.id.textview_welcome_connectedto);
        if(getBLEManager().isDeviceConnected()){
            connectedTextView.setText(getResources().getString(R.string.all_connectedTo, getBLEManager().getDeviceName()));
        }
        else{
            connectedTextView.setText(getResources().getString(R.string.all_notConnected));
        }
    }

    /**
     * Method to show a dialog to confirm that the user wants to connect to anocher device
     *
     * @param deviceClicked The targeted device that the user might want to connect to
     */
    private void showConnectToAnotherDeviceDialog(DeviceModel deviceClicked){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Are you sure you want to disconnect from " + getBLEManager().getDeviceName() + " and connect to " + deviceClicked.getName()+"?")
                .setPositiveButton("Yes", (dialog, which) -> {
                    disconnectDevice();
                    showConnectingDialog();
                    getBLEManager().initConnection(deviceClicked);
                    getBLEManager().stopScan();
                })
                .setNegativeButton("No", null)
                .show();
    }


    /**
     * Method to show a dialog to confirm the disconnection to the current connected device
     */
    private void showDisconnectDialog(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Are you sure you want to disconnect from " + getBLEManager().getDeviceName() + "?")
                .setPositiveButton("Yes", (dialog, which) -> {
                    disconnectDevice();
                    adapter.notifyDataSetChanged();
                })
                .setNegativeButton("No", null)
                .show();
    }

    /**
     * Method to start scanning for the devices in range
     */
    protected void scanForBLEdevices(){
        // We reset the adapter and remove the devices found in previous scans
        adapter.clear();

        if(mArrayBLEDevices==null)
            mArrayBLEDevices = new ArrayList<>();
        else
            mArrayBLEDevices.clear();

        // If there is a device already connected we add it to the list
        if(getBLEManager().isDeviceConnected()){
            mArrayBLEDevices.add(getBLEManager().getDevice());
            adapter.add(getBLEManager().getDevice());
        }

        adapter.notifyDataSetChanged();     // Update the ListView

        // We start scanning from a background thread to not block the UI and assign a ScanListener
        runOnUiThread(() -> getBLEManager().scanAvailableDevices(mWSDBleScanListener));
    }

    /**
     * Implementation of the ScanListener interface methods
     */
    protected WSDBleScanListener mWSDBleScanListener = new WSDBleScanListener() {
        /**
         * Method called when a device is scanned
         * @param device Device scanned
         */
        @Override
        public void onDeviceScanned(DeviceModel device) {
            if(device!=null){
                //We check that the device has not been already inserted in the list
                if(!BLEUtils.checkDeviceOnList(mArrayBLEDevices, device)) {
                    mArrayBLEDevices.add(device);
                    if(adapter!=null)
                        adapter.add(device);

                    adapter.notifyDataSetChanged();     // Refresh the list
                }
            }
        }

        /**
         * When the Scan timeout is reached we show in the log a summary with all devices found. For debugging purposes.
         * @param list  Input list with all devices scanned
         */
        @Override
        public void onDeviceScanTimeOut(List<DeviceModel> list) {
            Log.d(APPTAG, "Timeout reached. Summary:");

            for(int i=0; i<list.size();i++){
                Log.d(APPTAG, "Device name: " + list.get(i).getName() + "\n Device MAC address: " + list.get(i).getAddress() + "\n Device type: " + list.get(i).getDeviceType() + "\n \n");
            }
        }
    };

    /**
     * Method to disconnect from the device we are currently connected
     */
    private void disconnectDevice(){
        if(WSDBleManager.getInstance(getApplicationContext()).isDeviceConnected()) {
            WSDBleManager.getInstance(getApplicationContext()).disconnectDevice();
        }
        updateConnectedToInfo();    // Update the TextView with the connection details
    }

    /**
     * Method to disconnect the device (in case there is an active connection) and close the channel
     */
    private void closeConnection(){
        if(WSDBleManager.getInstance(getApplicationContext()).isDeviceConnected()) {
            WSDBleManager.getInstance(getApplicationContext()).disconnectDevice();
        }
        WSDBleManager.getInstance(getApplicationContext()).close();
        updateConnectedToInfo();    // Update the TextView with the connection details
    }


    //-------------- Redirecting methods --------------//
    /*
    // Methods used to move to other activities
    */

    private void goToDataDisplayActivity(){
        Intent dataDisplayIntent = new Intent(this, DataDisplayActivity.class);
        shouldCloseConnection = false;  // We should not close the connection as we are not leaving the app
        getBLEManager().stopScan();
        finish();
        startActivity(dataDisplayIntent);
    }

    private void goToSettingsActivity(){
        Intent settingsIntent = new Intent(this, SettingsActivity.class);
        shouldCloseConnection = false;  // We should not close the connection as we are not leaving the app
        finish();
        startActivity(settingsIntent);
    }

    private void goToTutorialActivity(){
        Intent settingsIntent = new Intent(this, TutorialActivity.class);
        shouldCloseConnection = false;  // We should not close the connection as we are not leaving the app
        finish();
        startActivity(settingsIntent);
    }


    //-------------- WSDBleCallback methods that needs overwritting ---------------//
    /*
     * Callback methods of the BLE Manager.
     */

    /**
     * Method called if the connection with the target device requires a bonding procedure
     * @param device Device that requires bonding
     */
    @Override
    public void onBondingRequired(BluetoothDevice device) {
        super.onBondingRequired(device);
        bondingRequired = true;     // We set this boolean to avoid redirecting to the DataDisplay before the bonding is achieved
        Toast.makeText(getApplicationContext(), "Please follow pairing instructions in Notification Bar", Toast.LENGTH_LONG).show();
    }

    /**
     * Method called when the bonding procedure is succesfully finished
     * @param device Device bonded
     */
    @Override
    public void onBonded(BluetoothDevice device) {
        super.onBonded(device);
        if(connectingDialog.isShowing()) {  //If the connecting dialog is shown we close it
            connectingDialog.dismiss();
        }
        getBLEManager().generateSessionID();    // We generate a sessionID for this connection
        if(bondingRequired) {
            if(MeasurementManager.getInstance().isCloudPostingEnabled(getApplicationContext()))     // If the cloud posting feature is enabled
                MeasurementManager.getInstance().startPostDataService(getApplicationContext());     // we start the posting background service
            goToDataDisplayActivity();                      // and we move to the DataDisplay screen
        }
    }


    /**
     * Method called when the device we are connecting to is ready
     * @param device Device ready
     */
    @Override
    public void onDeviceReady(BluetoothDevice device) {
        super.onDeviceReady(device);

        getBLEManager().generateSessionID();    // We generate a sessionID for this connection
        if(connectingDialog.isShowing()) {
            connectingDialog.dismiss();
        }
        if(!bondingRequired) {
            if(MeasurementManager.getInstance().isCloudPostingEnabled(getApplicationContext())) // We start the posting service if it is enabled
                MeasurementManager.getInstance().startPostDataService(getApplicationContext());
            goToDataDisplayActivity();
        }
        else{
            bondingRequired = false;
        }
    }

    @Override
    public void onError(BluetoothDevice device, String message, int errorCode) {
        super.onError(device, message, errorCode);
    }

    @Override
    public void onDeviceDisconnected(BluetoothDevice device) {
        super.onDeviceDisconnected(device);
    }
}




