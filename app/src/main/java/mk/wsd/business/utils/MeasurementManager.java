package mk.wsd.business.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;

import mk.wsd.R;
import mk.wsd.business.datamodel.MeasurementModel;
import mk.wsd.business.datamodel.MeasurementSetModel;
import mk.wsd.business.services.ClearDataService;
import mk.wsd.business.services.PostDataService;

import static android.content.Context.MODE_PRIVATE;
import static mk.wsd.business.activities.BLEActivity.APPTAG;

/**
 * The MeasurementManager helps us keeping track of the latest measurements for every sensor integrated
 * in the RapidIoT. We store the latest 4 measurements for every type. It also includes methods to
 * handle the services to post data and clear data from the cloud. It is designed as a singleton class.
 */
public class MeasurementManager {

    // Final integers to identify the measurement type depending on the sensor
    public static final int TYPE_TEMPERATURE = 1;
    public static final int TYPE_HUMIDITY = 2;
    public static final int TYPE_PRESSURE = 3;
    public static final int TYPE_LIGHT = 4;

    // We keep track of the latest 4 measurements
    private static final int ARRAY_LIMIT = 4;

    // We declare here the four ArrayList (one per measurement type) that store the latest 4 measurements
    private ArrayList<MeasurementModel> arrayTemperature = new ArrayList<>();
    private ArrayList<MeasurementModel> arrayHumidity = new ArrayList<>();
    private ArrayList<MeasurementModel> arrayPressure = new ArrayList<>();
    private ArrayList<MeasurementModel> arrayLight = new ArrayList<>();

    private int selectedType = TYPE_TEMPERATURE;    // To keep track of the selected type that is plotted in the table

    private static MeasurementManager mInstance = null;

    private MeasurementManager() {

    }

    public static synchronized MeasurementManager getInstance() {
        if (mInstance == null) {
            mInstance = new MeasurementManager();
        }
        return mInstance;
    }

    /**
     * To change the selected measurement type
     * @param type Type of measurement (int)
     */
    public void setSelectedType (int type){
        selectedType = type;
    }

    /**
     * To obtain the selected measurement type
     * @return Type of measurement (int)
     */
    public int getSelectedType (){
        return selectedType;
    }

    /**
     * To add a temperature measurement to the Temperature array
     * @param measurement The temperature measurement in the format of MeasurementModel
     */
    public void addTemperature(MeasurementModel measurement){
        if(arrayTemperature.size()==ARRAY_LIMIT)    // If the array is full we delete the oldest entry
            arrayTemperature.remove(0);

        arrayTemperature.add(measurement);      // and enqueue the new one
    }

    /**
     * To add a humidity measurement to the Temperature array
     * @param measurement The humidity measurement in the format of MeasurementModel
     */
    public void addHumidity(MeasurementModel measurement){
        if(arrayHumidity.size()==ARRAY_LIMIT)    // If the array is full we delete the oldest entry
            arrayHumidity.remove(0);

        arrayHumidity.add(measurement);      // and enqueue the new one
    }

    /**
     * To add a pressure measurement to the Temperature array
     * @param measurement The pressure measurement in the format of MeasurementModel
     */
    public void addPressure(MeasurementModel measurement){
        if(arrayPressure.size()==ARRAY_LIMIT)    // If the array is full we delete the oldest entry
            arrayPressure.remove(0);

        arrayPressure.add(measurement);      // and enqueue the new one
    }

    /**
     * To add a light measurement to the Temperature array
     * @param measurement The light measurement in the format of MeasurementModel
     */
    public void addLight(MeasurementModel measurement){
        if(arrayLight.size()==ARRAY_LIMIT)    // If the array is full we delete the oldest entry
            arrayLight.remove(0);

        arrayLight.add(measurement);      // and enqueue the new one
    }

    /**
     * Method that returns the array with the latest values of currently selected measurement type
     * @return Array with the values of the current measurement type
     */
    public ArrayList<MeasurementModel> getSelectedMeasurementArray(){
        switch (selectedType){
            case (TYPE_TEMPERATURE):
                return arrayTemperature;
            case (TYPE_HUMIDITY):
                return arrayHumidity;
            case (TYPE_PRESSURE):
                return arrayPressure;
            case (TYPE_LIGHT):
                return arrayLight;
            default:
                return arrayTemperature;
        }
    }

    /**
     * Method to reset and clean all measurements
     */
    public void clearMeasurements(){
        arrayTemperature.clear();
        arrayHumidity.clear();
        arrayPressure.clear();
        arrayLight.clear();
    }

    /**
     * Method that checks if we have at least one measurement of every type
     * @return  true if we have at least one measurement of each type
     */
    public boolean isMeasurementSetComplete(){
        return  (arrayTemperature.size()>0) &&
                (arrayHumidity.size()>0) &&
                (arrayPressure.size()>0) &&
                (arrayLight.size()>0);
    }

    /**
     * Method to retrieve the latest set of measurement from the arrays stored
     * @return The latest set of measurement, one of each type in the format of the MeasurementSetModel
     */
    public MeasurementSetModel getLatestSetOfMeasurements (){
        MeasurementSetModel measurementSetModel;
        if(isMeasurementSetComplete()) {
            measurementSetModel = new MeasurementSetModel(arrayTemperature.get(arrayTemperature.size()-1),
                                                          arrayHumidity.get(arrayHumidity.size()-1),
                                                          arrayPressure.get(arrayPressure.size()-1),
                                                          arrayLight.get(arrayLight.size()-1));
            return measurementSetModel;
        }
        else{
            return null;
        }
    }

    /**
     *  Method to obtain the latest timestamp received from a specific sensor
     * @param sensortype Type of measurement (int)
     * @return Latest timestamp (long)
     */
    public long getLatestTimestamp (int sensortype){
        switch(sensortype){
            case TYPE_HUMIDITY:
                if(arrayHumidity.size()!=0)
                    return arrayHumidity.get(arrayHumidity.size()-1).getTimeStampLong();
                else
                    return 0;

            case TYPE_PRESSURE:
                if(arrayPressure.size()!=0)
                    return arrayPressure.get(arrayPressure.size()-1).getTimeStampLong();
                else
                    return 0;

            case TYPE_TEMPERATURE:
                if(arrayTemperature.size()!=0)
                    return arrayTemperature.get(arrayTemperature.size()-1).getTimeStampLong();
                else
                    return 0;

            case TYPE_LIGHT:
                if(arrayLight.size()!=0)
                    return arrayLight.get(arrayLight.size()-1).getTimeStampLong();
                else
                    return 0;

            default:
                if(arrayTemperature.size()!=0)
                    return arrayTemperature.get(arrayTemperature.size()-1).getTimeStampLong();
                else
                    return 0;

        }
    }

    /**
     *  Method to obtain the related unit to be shown in the table depending on the currently selected
     *  measurement type.
     * @param temperatureUnitIsCelsius In case the current type is temperature this boolean indicates if the
     *      unit is in celsius or Fahrenheit. In case the current type is not temperature this parameter is irrelevant.
     * @return String with the unit as it must be shown to the user in the table
     */
    public String getUnit (boolean temperatureUnitIsCelsius){
        switch (selectedType) {
            case (MeasurementManager.TYPE_TEMPERATURE):
                if(temperatureUnitIsCelsius)
                    return " ºC";
                else
                    return " ºF";
            case (MeasurementManager.TYPE_HUMIDITY):
                return " %";
            case (MeasurementManager.TYPE_PRESSURE):
                return " hPa";
            case (MeasurementManager.TYPE_LIGHT):
                return " lux";
            default:
                return "";
        }
    }

    /**
     * Method to start the service to post data to the cloud
     * @param mContext Application context
     */
    public void startPostDataService(Context mContext){
        if(!isPostingServiceRunning(mContext)) {
            Intent intent = new Intent(mContext, PostDataService.class);
            Log.d(APPTAG, "WSDBleManager - Starting Service");
            mContext.startService(intent);
        }
    }

    /**
     * Method to check if the service for posting data is running in the background
     * @param mContext Application context
     * @return true if the service is running, false otherwise
     */
    public boolean isPostingServiceRunning(Context mContext){
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (PostDataService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to check if the CloudPosting feature is enabled by the user in the SharedPreferences
     * @param mContext Application context
     * @return true if it is enable, false otherwise
     */
    public boolean isCloudPostingEnabled (Context mContext){
        SharedPreferences mSharedPreferences = mContext.getSharedPreferences(mContext.getResources().getString(R.string.sharedPref_name), MODE_PRIVATE);
        return mSharedPreferences.getBoolean(mContext.getResources().getString(R
                .string.sharedPref_key_postingdata), false);
    }

    /**
     * Method to stop the service to post data to the cloud
     * @param mContext Application context
     */
    public void stopPostDataService(Context mContext){
        Intent intent = new Intent(mContext,PostDataService.class);
        Log.d(APPTAG, "WSDBleManager - StopCloud Service");
        mContext.stopService(intent);
    }

    /**
     * Method to start the service to clear data from the cloud
     * @param mContext Application context
     */
    public void startClearDataService(Context mContext){
        Intent intent = new Intent(mContext, ClearDataService.class);
        Log.d(APPTAG, "WSDBleManager - Starting ClearData Service");
        mContext.startService(intent);
    }

}
