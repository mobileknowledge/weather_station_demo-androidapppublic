package mk.wsd.business.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Class implementing methods to help handling and parsing the data coming from the RapidIoT
 */
public class MeasurementUtils {

    private static final double HUMIDITYDECIMALS = 2;

    /**
     * Method to convert a byte array to an integer
     * @param array Input byte array
     * @return Integer representation of the byte array
     */
    private static int convertToInt(byte[] array){
        int size = array.length;
        int total = 0;
        for(int i=0; i<size; i++){  // We process byte per byte
            int tmp = array[i];     // We get the signed integer from that byte
            if(tmp<0)
                tmp += 256;         // We make it unsigned

            // Depending on the position of the byte we apply a weight factor of 256^i and add it to the total
            total = total + tmp * (int) Math.pow((double) 256, (double) i);
        }
        return total;
    }

    /**
     * Method to convert the humidity data coming from the sensor into a float value
     * @param rawData byte array coming from the sensor
     * @return Converted value to float
     */
    public static float getHumidityMeasurement (byte[] rawData){
        int dataToInt = convertToInt(rawData);              // It comes with two decimals implicit
        int factor = (int) Math.pow(10, HUMIDITYDECIMALS);  // so we have to divide it in order to
        return (float) dataToInt / (float) factor;          // get the correct format.
    }

    /**
     * Method to convert the pressure data coming from the sensor into a float value
     * @param rawData byte array coming from the sensor
     * @return Converted value to float
     */
    public static float getPressureMeasurement (byte[] rawData){
        int dataToInt = convertToInt(rawData);
        return (float) dataToInt;
    }

    /**
     * Method to convert the temperature data coming from the sensor into a float value
     * @param rawData byte array coming from the sensor
     * @return Converted value to float
     */
    public static float getTemperatureMeasurement (byte[] rawData){
        byte[] rawMeasurement;
        int dataToInt, factor;
        float result;
        if(rawData[0]==0){
            if(rawData[3]!= (byte)0xFF) {           // Positive value
                rawMeasurement = new byte[3];
                rawMeasurement[0] = rawData[1];
                rawMeasurement[1] = rawData[2];
                rawMeasurement[2] = rawData[3];
                dataToInt = convertToInt(rawMeasurement);
                factor = (int) Math.pow(10, HUMIDITYDECIMALS);
                result = (float) dataToInt / (float) factor;
            }
            else{
                rawMeasurement = new byte[3];       // Negative value
                rawMeasurement[0] = (byte) (~rawData[1]);
                rawMeasurement[1] = (byte) (~rawData[2]);
                rawMeasurement[2] = (byte) (~rawData[3]);
                dataToInt = convertToInt(rawMeasurement) + 1; //+1 Because of the 2's complement
                factor = (int) Math.pow(10, HUMIDITYDECIMALS);
                result = (float) (-1) * dataToInt / (float) factor;
            }
        }
        else{
            result = 0;
        }
        return result;
    }

    /**
     * Method to convert the light data coming from the sensor into a float value
     * @param rawData byte array coming from the sensor
     * @return Converted value to float
     */
    public static float getLightMeasurement (byte[] rawData){
        float light = ByteBuffer.wrap(rawData).order(ByteOrder.LITTLE_ENDIAN).getFloat();
        return (float)(((float)Math.round(light*100))/100.0);
    }
}
