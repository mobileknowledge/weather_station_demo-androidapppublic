package mk.wsd.business.utils;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import mk.wsd.R;
import static android.support.v4.view.ViewPager.OnPageChangeListener;

/**
 * With this class we handle an Indicator of the page that is displayed in the Tutorial activity.
 */
public class ViewPagerIndicator extends LinearLayout {

    private final static int DEFAULT_INDICATOR_WIDTH = 5;
    private ViewPager viewpager;
    private int indicatorMargin = -1;
    private int indicatorWidth = -1;
    private int indicatorHeight = -1;
    private int animatorResId = R.animator.viewpager_indicator_scale_alpha;
    private int animatorReverseResId = 0;
    private int indicatorBackgroundResId = R.drawable.white_radius;
    private int indicatorUnselectedBackgroundResId = R.drawable.white_radius;
    private Animator animatorOut;
    private Animator animatorIn;
    private Animator immediateAnimatorOut;
    private Animator immediateAnimatorIn;

    private int lastPosition = -1;

    // <--------------- Constructors-------------------->

    public ViewPagerIndicator(Context context) {
        super(context);
        init(context, null);
    }

    public ViewPagerIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ViewPagerIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ViewPagerIndicator(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    // <--------------- Proprietary methods -------------------->

    // Method to initialized the indicator
    private void init(Context context, AttributeSet attrs) {
        handleTypedArray(context, attrs);
        checkIndicatorConfig(context);
    }

    /**
     * To extract the dimension/position parameters from the application attributes
     */
    private void handleTypedArray(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ViewPagerIndicator);
        indicatorWidth = typedArray.getDimensionPixelSize(R.styleable.ViewPagerIndicator_indicator_width, -1);
        indicatorHeight = typedArray.getDimensionPixelSize(R.styleable.ViewPagerIndicator_indicator_height, -1);
        indicatorMargin = typedArray.getDimensionPixelSize(R.styleable.ViewPagerIndicator_indicator_margin, -1);

        animatorResId = typedArray.getResourceId(R.styleable.ViewPagerIndicator_indicator_animator, R.animator.viewpager_indicator_scale_alpha);
        animatorReverseResId = typedArray.getResourceId(R.styleable.ViewPagerIndicator_indicator_animator_reverse, 0);
        indicatorBackgroundResId = typedArray.getResourceId(R.styleable.ViewPagerIndicator_indicator_drawable, R.drawable.white_radius);
        indicatorUnselectedBackgroundResId = typedArray.getResourceId(R.styleable.ViewPagerIndicator_indicator_drawable_unselected, indicatorBackgroundResId);

        int orientation = typedArray.getInt(R.styleable.ViewPagerIndicator_indicator_orientation, -1);
        setOrientation(orientation == VERTICAL ? VERTICAL : HORIZONTAL);

        int gravity = typedArray.getInt(R.styleable.ViewPagerIndicator_indicator_gravity, -1);
        setGravity(gravity >= 0 ? gravity : Gravity.CENTER);

        typedArray.recycle();
    }

    /**
     * To check the validity of the layout parameters
     */
    private void checkIndicatorConfig(Context context) {
        indicatorWidth = (indicatorWidth < 0) ? dip2px(DEFAULT_INDICATOR_WIDTH) : indicatorWidth;
        indicatorHeight = (indicatorHeight < 0) ? dip2px(DEFAULT_INDICATOR_WIDTH) : indicatorHeight;
        indicatorMargin = (indicatorMargin < 0) ? dip2px(DEFAULT_INDICATOR_WIDTH) : indicatorMargin;

        animatorResId = (animatorResId == 0) ? R.animator.viewpager_indicator_scale_alpha : animatorResId;

        animatorOut = createAnimatorOut(context);
        immediateAnimatorOut = createAnimatorOut(context);
        immediateAnimatorOut.setDuration(0);

        animatorIn = createAnimatorIn(context);
        immediateAnimatorIn = createAnimatorIn(context);
        immediateAnimatorIn.setDuration(0);

        indicatorBackgroundResId = (indicatorBackgroundResId == 0) ? R.drawable.white_radius : indicatorBackgroundResId;
        indicatorUnselectedBackgroundResId = (indicatorUnselectedBackgroundResId == 0) ? indicatorBackgroundResId : indicatorUnselectedBackgroundResId;
    }

    private Animator createAnimatorOut(Context context) {
        return AnimatorInflater.loadAnimator(context, animatorResId);
    }

    private Animator createAnimatorIn(Context context) {
        Animator animatorIn;
        if (animatorReverseResId == 0) {
            animatorIn = AnimatorInflater.loadAnimator(context, animatorResId);
            animatorIn.setInterpolator(new ReverseInterpolator());
        } else {
            animatorIn = AnimatorInflater.loadAnimator(context, animatorReverseResId);
        }
        return animatorIn;
    }

    /**
     * To assign a ViewPager and create the Indicator. It also assign the listeners for the ViewPager
     * @param viewPager Viewpager that will be linked to the indicator
     */
    public void setViewPager(ViewPager viewPager) {
        viewpager = viewPager;
        if (viewpager != null && viewpager.getAdapter() != null) {
            lastPosition = -1;
            createIndicators();
            viewpager.removeOnPageChangeListener(internalPageChangeListener);
            viewpager.addOnPageChangeListener(internalPageChangeListener);
            internalPageChangeListener.onPageSelected(viewpager.getCurrentItem());
        }
    }

    /**
     * Listener that is designer to get the events on a page change
     */
    private final OnPageChangeListener internalPageChangeListener = new OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override public void onPageSelected(int position) {

            if (viewpager.getAdapter() == null || viewpager.getAdapter().getCount() <= 0) {
                return;
            }

            if (animatorIn.isRunning()) {
                animatorIn.end();
                animatorIn.cancel();
            }

            if (animatorOut.isRunning()) {
                animatorOut.end();
                animatorOut.cancel();
            }

            View currentIndicator;
            if (lastPosition >= 0 && (currentIndicator = getChildAt(lastPosition)) != null) {
                currentIndicator.setBackgroundResource(indicatorUnselectedBackgroundResId);
                animatorIn.setTarget(currentIndicator);
                animatorIn.start();
            }

            View selectedIndicator = getChildAt(position);
            if (selectedIndicator != null) {
                selectedIndicator.setBackgroundResource(indicatorBackgroundResId);
                animatorOut.setTarget(selectedIndicator);
                animatorOut.start();
            }
            lastPosition = position;
        }

        @Override public void onPageScrollStateChanged(int state) {
        }
    };

    /**
     * @deprecated User ViewPager addOnPageChangeListener
     */
    @Deprecated public void setOnPageChangeListener(OnPageChangeListener onPageChangeListener) {
        if (viewpager == null) {
            throw new NullPointerException("can not find Viewpager , setViewPager first");
        }
        viewpager.removeOnPageChangeListener(onPageChangeListener);
        viewpager.addOnPageChangeListener(onPageChangeListener);
    }

    /**
     * To create the graphical indicators
     */
    @SuppressWarnings("ConstantConditions")
    private void createIndicators() {
        removeAllViews();
        int count = viewpager.getAdapter().getCount();
        if (count <= 0) {
            return;
        }
        int currentItem = viewpager.getCurrentItem();
        int orientation = getOrientation();

        for (int i = 0; i < count; i++) {
            if (currentItem == i) {
                addIndicator(orientation, indicatorBackgroundResId, immediateAnimatorOut);
            } else {
                addIndicator(orientation, indicatorUnselectedBackgroundResId, immediateAnimatorIn);
            }
        }
    }

    private void addIndicator(int orientation, @DrawableRes int backgroundDrawableId,
                              Animator animator) {
        if (animator.isRunning()) {
            animator.end();
            animator.cancel();
        }

        View Indicator = new View(getContext());
        Indicator.setBackgroundResource(backgroundDrawableId);
        addView(Indicator, indicatorWidth, indicatorHeight);
        LayoutParams lp = (LayoutParams) Indicator.getLayoutParams();

        if (orientation == HORIZONTAL) {
            lp.leftMargin = indicatorMargin;
            lp.rightMargin = indicatorMargin;
        } else {
            lp.topMargin = indicatorMargin;
            lp.bottomMargin = indicatorMargin;
        }

        Indicator.setLayoutParams(lp);

        animator.setTarget(Indicator);
        animator.start();
    }

    private class ReverseInterpolator implements Interpolator {
        @Override public float getInterpolation(float value) {
            return Math.abs(1.0f - value);
        }
    }

    public int dip2px(float dpValue) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }


}
