package mk.wsd.business.utils;

import android.bluetooth.BluetoothAdapter;
import java.util.ArrayList;
import mk.wsd.business.datamodel.DeviceModel;

public class BLEUtils {

    public final static int REQUEST_ENABLE_BT = 1;

    /**
     * Method to check that the Bluetooth adapter
     * @return  true if the adapter is enabled, false otherwise
     */
    public static boolean isBluetoothEnabled(){
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();  //Obtain the Bluetooth Adapter
        return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
    }

    /**
     * Method to check if a device is on a list of devices
     * @param devicesList  Input parameter with the devices list
     * @param newDevice  The device to be checked
     * @return  true if the device is on the list, false otherwise
     */
    public static boolean checkDeviceOnList (ArrayList<DeviceModel> devicesList, DeviceModel newDevice){
        for(int i=0; i<devicesList.size(); i++){
            if(devicesList.get(i).getAddress().equals(newDevice.getAddress()))
                return true;
        }
        return false;
    }
}
